![clougistic](https://bitbucket.org/clougistic/magento2-connector/avatar/128)

## Clougistic WMS Connector for Magento 2

Install the Magento Clougistic WMS Connector extension

- 	This extension can only be used with Magento 2.
-	Backup your Magento installation and database before installation.
-	Git clone https://bitbucket.org/clougistic/magento2-connector.git or [download](https://bitbucket.org/clougistic/magento2-connector/downloads/) this repo in the root of your Magento installation.
-	Run commands:
	-
	-
-	Logout and back in to the admin panel.
-	Configuration is under Stores > Configuration > Clougistic

# Help

More information about installation and configuration can be found in the [online help](http://help.clougistic.com/magento2-extension).