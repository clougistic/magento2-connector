<?php
/**
 * @author Clougistic
 * @copyright Copyright (c) 2018 Clougistic (https://www.toverteam.nl)
 * @package Amasty_HelloWorld
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Clougistic_Connector',
    __DIR__
);