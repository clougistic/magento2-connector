<?php namespace Clougistic\Connector\Setup;
 
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface; 
 
class InstallData implements InstallDataInterface
{
    protected $_eavSetupFactory;
    
    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
         $this->_eavSetupFactory = $eavSetupFactory;
    }
    
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
        
        # add customer consignment type
        $eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY,
            'consignment_type', [
                'group' => 'Consignment',
                'label' => 'Consignment Type',
                'source' => 'Clougistic\Connector\Model\System\Config\Source\Consignment\Type',
                'type' => 'int',
                'input' => 'select',
                'required' => false,
                'visible' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'system' => 0,
                'adminhtml_only' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'user_defined' => true,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'position' => 500,
        ]);
        
        # add customer consignment zone
        $eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY,
            'consignment_zone', [
                'group' => 'Consignment',
                'label' => 'Consignment Zone',
                'type' => 'int',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'system' => 0,
                'adminhtml_only' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'user_defined' => true,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'position' => 510,
        ]);
        
        $setup->endSetup();
    }
}