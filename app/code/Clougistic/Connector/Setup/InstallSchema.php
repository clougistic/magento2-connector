<?php namespace Clougistic\Connector\Setup;
 
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;
 
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();

        # add cg_status to orders
        $connection->addColumn($setup->getTable('sales_order'), 
            'cg_status', [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => false, 
                'default' => 0,
                'comment' => 'Cg Status'
        ]);

        # add cg_status to order grid
        $connection->addColumn($setup->getTable('sales_order_grid'), 
            'cg_status', [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => false, 
                'default' => 0,
               'comment' => 'Cg Status'
        ]);
        
        # add cg_status to products
        $connection->addColumn($setup->getTable('catalog_product_entity'), 
            'cg_status', [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => false, 
                'default' => 0,
               'comment' => 'Cg Status'
        ]);

        # add cg_cost to order items
        $connection->addColumn($setup->getTable('sales_order_item'), 
            'cg_cost', [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => false, 
                'default' => 0,
                'comment' => 'Cg Cost'
        ]);
 
        $setup->endSetup();
    }
}