<?php namespace Clougistic\Connector\Helper;

/**
 * Utility Helper
 *
 * @category   Clougistic
 * @package    Clougistic_Connector
 * @author     Daniel van Els
 * @website    http://www.clougistic.com
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const MODULE_KEY = 'Clougistic_Connector';
    
    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $_moduleList;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Module\ModuleListInterface $moduleList
    ) {
        $this->_moduleList = $moduleList;

        parent::__construct($context);
    }

    public function getExtensionVersion()
    {
        return $this->_moduleList->getOne(self::MODULE_KEY)['setup_version'];
    }
}