<?php namespace Clougistic\Connector\Helper\PrintNode;
/**
 * Api Helper
 *
 * @category   Clougistic
 * @package    Clougistic_Connector
 * @author     Daniel van Els
 * @website    http://www.clougistic.com
 */
 
use \Magento\Framework\App\Helper\AbstractHelper; 

class Config extends AbstractHelper
{
    /**
     * injected dependencies
     */
    protected $_scopeConfig;
    
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;

        parent::__construct($context);
    }
    
    /**
     * Backwards compatible config reader
     */
    private function getStoreConfig($key, $store = null) 
    {
        return $this->_scopeConfig->getValue($key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }
    
	/**
     * @param null $store
     * @return bool
     */
	public function isEnabled($store = null)
	{
		return (bool)$this->getStoreConfig('clougistic_connector/printnode/enabled', $store);
	}
	
	/**
     * @param null $store
     * @return mixed
     */
	public function getEnabledDocuments($store = null)
	{
		return explode(',', $this->getStoreConfig('clougistic_connector/printnode/documents', $store));
	}

	/**
     * @param $key
     * @param $document
     * @param null $store
     * @return bool
     */
	public function canPrintDocument($type, $document, $store = null)
	{
		$enabledDocuments = $this->getEnabledDocuments($store);
		$key = sprintf('%s_%s', $type, str_replace(' ', '_', strtolower($document)));
		return in_array($key, $enabledDocuments);
	}
}