<?php namespace Clougistic\Connector\Helper;
/**
 * Api Helper
 *
 * @category   Clougistic
 * @package    Clougistic_Connector
 * @author     Daniel van Els
 * @website    http://www.clougistic.com
 */
 
use \Magento\Framework\App\Helper\AbstractHelper; 
 
class Api extends AbstractHelper
{
    const ERROR_TITLE = 'Clougistic Error';
    const ERROR_LOGFILE = 'clougistic_connector.log';
    const USER_AGENT = 'Clougistic/Connector';
    const URL_API_VERSION = '/api/v1';
    
    # clougistic order statuses
	const ORDER_STATUS_NEW = 0;
	const ORDER_STATUS_PROCESSING = 1;
	const ORDER_STATUS_SHIPPED = 2;
	const ORDER_STATUS_ON_HOLD = 8;
	const ORDER_STATUS_CANCELED = 9;

    const ORDER_TYPE_SALE = 0;
    const ORDER_TYPE_SALE_POS = 1;
	
	# order sync statuses
	const ORDER_UNSYNCED = 0;
	const ORDER_INVALID = 1;
	const ORDER_ERROR = 2;
	const ORDER_SYNCED = 3;

	# product sync statuses
	const PRODUCT_UNSYNCED = 0;
	const PRODUCT_ERROR = 1;
	const PRODUCT_SYNCED = 2;
	
    const FIELD_ITEMS = 'items';
	const FIELD_USE_REFERENCE = 'use_reference';
	
	# order head fields
    const ORDER_FIELD_REFERENCE = 'reference';
    const ORDER_FIELD_CUSTOMER_ID = 'customer_id';
    const ORDER_FIELD_SOURCE_ID = 'source_id';
    const ORDER_FIELD_TYPE = 'type';
    const ORDER_FIELD_CUSTOM = 'custom_field';
    const ORDER_FIELD_COD_AMOUNT = 'cod_amount';
    const ORDER_FIELD_CURRENCY = 'currency';
    const ORDER_FIELD_STATUS = 'status';
    const ORDER_FIELD_FLAG_PARTIAL = 'flag_partial';
    const ORDER_FIELD_SHIPPING_METHOD = 'shipping_method';
    const ORDER_FIELD_OPTIONS = 'options';
    const ORDER_FIELD_OPTIONS_POS_ZONE_ID = '@pos_zone_id';
    const ORDER_FIELD_OPTIONS_CONSIGNMENT_ZONE_ID = '@consignment_zone_id';
    const ORDER_FIELD_CREATED_AT = 'created_at';
	const ORDER_FIELD_DELIVERY_AT = 'delivery_at';

	# clougistic pickup shipping method
	const ORDER_SHIPPING_METHOD_PICKUP = 'pickup';
	
	# billing address fields
    const ORDER_FIELD_BILLING_COMPANY = 'billing_company';
    const ORDER_FIELD_BILLING_FIRSTNAME = 'billing_firstname';
    const ORDER_FIELD_BILLING_LASTNAME = 'billing_lastname';
    const ORDER_FIELD_BILLING_STREET_FULL = 'billing_street_full';
    const ORDER_FIELD_BILLING_STREET = 'billing_street';
    const ORDER_FIELD_BILLING_HOUSENUMBER = 'billing_housenumber';
    const ORDER_FIELD_BILLING_HOUSENUMBER_EXT = 'billing_housenumber_ext';
    const ORDER_FIELD_BILLING_POSTCODE = 'billing_postcode';
    const ORDER_FIELD_BILLING_CITY = 'billing_city';
    const ORDER_FIELD_BILLING_REGION = 'billing_region';
    const ORDER_FIELD_BILLING_COUNTRY = 'billing_country';
    const ORDER_FIELD_BILLING_PHONE = 'billing_phone';
    const ORDER_FIELD_BILLING_EMAIL = 'billing_email';
	
	# shipping address fields
    const ORDER_FIELD_SHIPPING_COMPANY = 'shipping_company';
    const ORDER_FIELD_SHIPPING_FIRSTNAME = 'shipping_firstname';
    const ORDER_FIELD_SHIPPING_LASTNAME = 'shipping_lastname';
    const ORDER_FIELD_SHIPPING_STREET_FULL = 'shipping_street_full';
    const ORDER_FIELD_SHIPPING_STREET = 'shipping_street';
    const ORDER_FIELD_SHIPPING_HOUSENUMBER = 'shipping_housenumber';
    const ORDER_FIELD_SHIPPING_HOUSENUMBER_EXT = 'shipping_housenumber_ext';
    const ORDER_FIELD_SHIPPING_POSTCODE = 'shipping_postcode';
    const ORDER_FIELD_SHIPPING_CITY = 'shipping_city';
    const ORDER_FIELD_SHIPPING_REGION = 'shipping_region';
    const ORDER_FIELD_SHIPPING_COUNTRY = 'shipping_country';
    const ORDER_FIELD_SHIPPING_PHONE = 'shipping_phone';
    const ORDER_FIELD_SHIPPING_EMAIL = 'shipping_email';

	# order item fields
    const ORDER_ITEM_FIELD_SKU = 'sku';
    const ORDER_ITEM_FIELD_REFERENCE = 'reference';
    const ORDER_ITEM_FIELD_QTY = 'qty';
    const ORDER_ITEM_FIELD_WEIGHT = 'weight';
    const ORDER_ITEM_FIELD_DESCRIPTION = 'description';
    const ORDER_ITEM_FIELD_UNIT_PRICE = 'unit_price';
    const ORDER_ITEM_FIELD_CUSTOM_OPTIONS = 'custom_options';
    const ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA = 'option_data';
    const ORDER_ITEM_FIELD_CUSTOM_OPTION_SHIP_SEPARATELY = 'ship_separately';

	const ORDER_RMA_FIELD_REFERENCE = 'rma_reference';
	const ORDER_RMA_FIELD_USE_ITEM_REFERENCE = 'use_item_reference';
	const ORDER_RMA_FIELD_CANCEL_ITEMS = 'cancel_items';
	    
    /**
     * injected dependencies
     */
    protected $_scopeConfig;
    protected $_carrierSelector;
    protected $_appState;
    protected $_messageManager;
    protected $_catalogProduct;
    protected $_catalogProductType;
    protected $_catalogProductOption;
    protected $_catalogProductOptionValue;
    protected $_customer;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    // api request errors
    protected $_errors;

    // response
    protected $_response;
    
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Catalog\Model\Product $catalogProduct
     * @param \Magento\Catalog\Model\Product\Type $catalogProductType
     * @param \Magento\Catalog\Model\Product\Option $catalogProductOption
     * @param \Magento\Catalog\Model\Product\Option\Value $catalogProductOptionValue
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Clougistic\Connector\Model\Carrier\Selector $carrierSelector
     * @param \Clougistic\Connector\Model\Api\Response $apiResponse
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\State $appState,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Catalog\Model\Product $catalogProduct,
        \Magento\Catalog\Model\Product\Type $catalogProductType,
        \Magento\Catalog\Model\Product\Option $catalogProductOption,
        \Magento\Catalog\Model\Product\Option\Value $catalogProductOptionValue,
        \Magento\Customer\Model\Customer $customer,
        \Clougistic\Connector\Model\Carrier\Selector $carrierSelector,
        \Clougistic\Connector\Model\Api\Response $apiResponse,
        \Magento\Backend\Model\UrlInterface $backendUrl
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_response = $apiResponse;
        $this->_appState = $appState;
        $this->_messageManager = $messageManager;
        $this->_catalogProduct = $catalogProduct;
        $this->_catalogProductType = $catalogProductType;
        $this->_catalogProductOption = $catalogProductOption;
        $this->_catalogProductOptionValue = $catalogProductOptionValue;
        $this->_customer = $customer;
        $this->_carrierSelector = $carrierSelector;
        $this->_backendUrl = $backendUrl;

        parent::__construct($context);
    }

    /**
     * Backwards compatible config reader
     */
    private function getStoreConfig($key, $store = null) 
    {
        return $this->_scopeConfig->getValue($key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }
    
    /**
     * @return bool
     */
	public function isAdmin()
	{
		return $this->_appState->getAreaCode() === 'adminhtml';
	}
    
    /**
     * @param null $store
     * @return mixed
     */
    public function isEnabled($store = null)
    {
        return (bool)$this->getStoreConfig('clougistic_connector/general/enabled', $store);
    }
	
	/**
     * @param null $store
     * @return mixed
     */
    public function isDebug()
    {
        return (bool)$this->getStoreConfig('clougistic_connector/general/debug_mode');
    }
	
    /**
     * @param null $store
     * @return mixed
     */
	public function getCODMethods()
	{
		return explode(',', $this->getStoreConfig('clougistic_connector/general/cod_paymentmethods'));
	}
	
	/**
     * @param null $store
     * @return mixed
     */
	public function getPickupMethods()
	{
		return explode(',', $this->getStoreConfig('clougistic_connector/general/pickup_shippingmethods'));
	}
	
	/**
     * @param null $store
     * @return mixed
     */
	public function getExcludeShippingMethods()
	{
		return explode(',', $this->getStoreConfig('clougistic_connector/general/exclude_shippingmethods'));
	}

	/**
     * @param null $store
     * @return mixed
     */
	public function getExcludeCustomOptions($store = null)
	{
		return explode("\n", $this->getStoreConfig('clougistic_connector/general/exclude_custom_options', $store));
	}
	
    /**
     * @param null $store
     * @return mixed
     */
    public function getUrlEndPoint($store = null)
    {
        return $this->getStoreConfig('clougistic_connector/general/url_endpoint', $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getApiKey($store = null)
    {
        return $this->getStoreConfig('clougistic_connector/general/api_key', $store);
    }
	
	/**
     * @param null $store
     * @return mixed
     */
    public function getSourceId($store = null)
    {
        return $this->getStoreConfig('clougistic_connector/general/source_id', $store);
    }

	/**
     * @param null $store
     * @return mixed
     */
    public function getEntityId($store = null)
    {
        return $this->getStoreConfig('clougistic_connector/general/entity_id', $store);
    }

	/**
     * @param null $store
     * @return mixed
     */
    public function getOrderDeliveryDateField($store = null)
    {
        return $this->getStoreConfig('clougistic_connector/general/order_delivery_date_field', $store);
    }

	/**
     * @param null $store
     * @return mixed
     */	
	public function getProductCategoryField($store = null)
	{
		return $this->getStoreConfig('clougistic_connector/general/product_category_field', $store);
	}

	/**
     * @param null $store
     * @return mixed
     */	
	public function getShipmentIncrementIdPrefix($store = null)
	{
		return $this->getStoreConfig('clougistic_connector/general/shipment_increment_id_prefix', $store);
	}

	/**
     * @param null $store
     * @return bool
     */
	public function isPOSEnabled($store = null)
	{
		return (bool)$this->getStoreConfig('clougistic_connector/pos/enabled', $store);
	}
	
	/**
     * @param null $store
     * @return bool
     */
	public function getPOSZoneId($store = null)
	{
		return $this->getStoreConfig('clougistic_connector/pos/zone_id', $store);
	}
	
	/**
     * @param null $store
     * @return mixed
     */
	public function getPOSShippingMethods($store = null)
	{
		return explode(',', $this->getStoreConfig('clougistic_connector/pos/shippingmethods', $store));
	}

	/**
     * @param $order
     * @return bool
     */
	public function isPOSOrder(\Magento\Sales\Model\Order $order)
	{
		$posShippingMethods = $this->getPOSShippingMethods($order->getStore());
		$shippingMethod = explode('_', $order->getShippingMethod())[0];
		return in_array($shippingMethod, $posShippingMethods);
	}
	
    /** 
	 * @param array $data
	 * @return bool
	 */
	public function isValidData(array $data = [])
	{
		return count($data[self::FIELD_ITEMS]) > 0;
	}
    
	/**
	 * Get carrier selector
	 */
	public function getCarrierSelector() 
	{
        return $this->_carrierSelector;
	}
    
        /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function prepareSaveOrder(\Magento\Sales\Model\Order $order)
    {
        $result = $this->getOrderHead($order) +
            $this->getBillingAddress($order->getBillingAddress()) +
            $this->getShippingAddress($order->getShippingAddress());
			
        return $result;
    }
	
	/**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function prepareUpdateOrder(\Magento\Sales\Model\Order $order)
    {
        $result = [
				self::ORDER_FIELD_REFERENCE => $order->getIncrementId(),
				self::FIELD_USE_REFERENCE => true,
			] +
            $this->getBillingAddress($order->getBillingAddress()) +
            $this->getShippingAddress($order->getShippingAddress());
		
		# in case the extra order options changed, we update them
		$options = null;
		
		if ($carrierOptions = $this->getCarrierSelector()->getOrderOptions($order)) {
			$options = (array)$options + $carrierOptions;
		}
		
		if ($consignmentData = $this->getOrderConsignmentData($order)) {
			if ($consignmentData->type == 'supply') {
				$options = (array)$options + [
					self::ORDER_FIELD_OPTIONS_CONSIGNMENT_ZONE_ID => $consignmentData->zone
				];
			}
		} 
		
		$result[self::ORDER_FIELD_OPTIONS] = $options;
				
		$status = $this->getStatus($order);

		if ($status !== null) {
			$result += [self::ORDER_FIELD_STATUS => $status];
		}

        return $result;
    }
	
	/**
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @return array
     */
	public function prepareSaveOrderRma(\Magento\Sales\Model\Order\Creditmemo $creditmemo) 
	{
		$order = $creditmemo->getOrder();
        $result = [
				self::ORDER_RMA_FIELD_REFERENCE => $creditmemo->getIncrementId(),
				self::ORDER_FIELD_REFERENCE => $order->getIncrementId(),
				self::FIELD_USE_REFERENCE => true,
				self::ORDER_RMA_FIELD_CANCEL_ITEMS => true,
				self::FIELD_ITEMS => $this->getOrderRmaItems($creditmemo)
			];
	
        return $result;
	}
	
    /**
     * @param $order
     * @return array
     */
    private function getOrderHead (\Magento\Sales\Model\Order $order)
    {
        $result = [];
        if ($order) {
            $result = [
                self::ORDER_FIELD_REFERENCE 		=> $order->getIncrementId(),
                self::ORDER_FIELD_SOURCE_ID 		=> $this->getSourceId($order->getStore()),
                self::ORDER_FIELD_CUSTOMER_ID 		=> (int)$order->getCustomerId(),
                self::ORDER_FIELD_COD_AMOUNT 		=> (float)$this->getCODAmount($order),
                self::ORDER_FIELD_CURRENCY 			=> $order->getStore()->getBaseCurrencyCode(),
                self::ORDER_FIELD_SHIPPING_METHOD 	=> $this->getShippingMethod($order),
                self::ORDER_FIELD_FLAG_PARTIAL 		=> (int)$order->getCanShipPartially(),
                self::ORDER_FIELD_CREATED_AT		=> $order->getCreatedAt(),
                self::ORDER_FIELD_DELIVERY_AT 		=> $this->getOrderDeliveryDate($order),
                self::FIELD_ITEMS 					=> $this->getOrderItems($order)
            ];

			$consignmentData = $this->getOrderConsignmentData($order);
			$isConsignmentSale = false;
			if ($consignmentData) {
				$isConsignmentSale = $consignmentData->type == 'sale';
			}

			$posEnabled = $this->isPOSEnabled($order->getStore());
			if ($posEnabled && $this->isPOSOrder($order)) {
				$result[self::ORDER_FIELD_TYPE] = self::ORDER_TYPE_SALE_POS;
				$result[self::ORDER_FIELD_OPTIONS] = [
					self::ORDER_FIELD_OPTIONS_POS_ZONE_ID => $this->getPOSZoneId($order->getStore())
				];
			}
			elseif ($isConsignmentSale) {
				$result[self::ORDER_FIELD_TYPE] = self::ORDER_TYPE_SALE_POS;
				$result[self::ORDER_FIELD_OPTIONS] = [
					self::ORDER_FIELD_OPTIONS_POS_ZONE_ID => $consignmentData->zone
				];
			}			
			else {
				$result[self::ORDER_FIELD_TYPE] = self::ORDER_TYPE_SALE;

				$options = null;
				
				if ($carrierOptions = $this->getCarrierSelector()->getOrderOptions($order)) {
					$options = (array)$options + $carrierOptions;
				}
				
				if ($consignmentData) {
					$options = (array)$options + [
						self::ORDER_FIELD_OPTIONS_CONSIGNMENT_ZONE_ID => $consignmentData->zone
					];
				} 
				
				$result[self::ORDER_FIELD_OPTIONS] = $options;
					
				if (($status = $this->getStatus($order)) !== null) {
					$result[self::ORDER_FIELD_STATUS] = $status;
				}
			}
        }
		
        return $result;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     */
	private function getStatus(\Magento\Sales\Model\Order $order)
	{
		# if items are cancelled, order status can still be complete
		# order in clougistic has to be cancelled if this is the case
		# order items can not be cancelled individually, complete order only
		# this has nothing to do with creditmemos as these use qty_refunded
		
		$sumCancelled = 0;
		foreach ($order->getAllItems() as $item) {
			$sumCancelled += $item->getQtyCanceled();
		}
		if ($sumCancelled > 0) {
			return self::ORDER_STATUS_CANCELED;
		}
		
		# return conversion from magento state > cg status
		
		switch ($order->getState()) {
			case $order::STATE_NEW:
			case $order::STATE_PENDING_PAYMENT:
			case $order::STATE_PAYMENT_REVIEW:
				return self::ORDER_STATUS_NEW;
			case $order::STATE_PROCESSING:
				return self::ORDER_STATUS_PROCESSING;
			case $order::STATE_HOLDED:
				return self::ORDER_STATUS_ON_HOLD;
			case $order::STATE_CANCELED:
			case $order::STATE_CLOSED:
				return self::ORDER_STATUS_CANCELED;
			default:
				return null;
		}
	}
	
    /**
     * @param Mage_Sales_Model_Order $order
	 * @return array
     */
    private function getOrderItems (\Magento\Sales\Model\Order $order)
    {
        $result = [];
        foreach ($order->getAllItems() as $item)
        {
            switch ($item->getProductType()) {
                case $this->_catalogProductType::TYPE_BUNDLE:

					$container = [];
					$orderItem = $this->getOrderItem($item, 'bundle-head');
					if ($orderItem !== null) {
						$container[] = $orderItem;
					}
					
					$relatedItems = $item->getChildrenItems();
					$relatedItemsCount = 0;
					if (count($relatedItems) > 0) {
						foreach ($relatedItems as $relatedItem) {
							$orderItem = $this->getOrderItem($relatedItem, 'bundle-item');
							if ($orderItem !== null) {
								if (count($container) > 0) {
									$container[] = $orderItem;
								}
								else {
									$result[] = [$orderItem];
								}
								$relatedItemsCount++;
							}
						}

						if (count($container) > 0 && $relatedItemsCount > 0) {
							$result[] = $container;
						}
					}
					
                break;
				case $this->_catalogProductType::TYPE_CONFIGURABLE:

					$relatedItems = $item->getChildrenItems();
					if (count($relatedItems) > 0) {
						$orderItem = $this->getOrderItem($relatedItems[0], 'configurable-item');
						if ($orderItem !== null) {
							$result[] = [$orderItem];
						}
                    }

				break;
                case $this->_catalogProductType::TYPE_SIMPLE:
                case $this->_catalogProductType::TYPE_GROUPED:
				
					if ($item->getParentItemId()) {
                        continue;
                    }
					
					$orderItem = $this->getOrderItem($item, 'simple-item');
					if ($orderItem !== null) {
						$result[] = [$orderItem];
					}
					
                break;
                default:
                    # do not sync virtual and downloadable items
                break;
            }
        }

        return $result;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $item
     * @return array
     */
    private function getOrderItem (\Magento\Sales\Model\Order\Item $item, $type)
    {
        $result = [];
        if ($item) 
		{
            $result = [
                self::ORDER_ITEM_FIELD_DESCRIPTION => $item->getName(),
                self::ORDER_ITEM_FIELD_WEIGHT => (float)$item->getWeight(),
                self::ORDER_ITEM_FIELD_QTY => (float)max($item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled() - $item->getQtyShipped(), 0)
            ];
			
			switch ($type) {
				case 'bundle-head':
				
					if ($this->getShipSeparately($item)) {
						return null;
					}
					
					$result[self::ORDER_ITEM_FIELD_SKU] = $item->getSku();
					$result[self::ORDER_ITEM_FIELD_REFERENCE] = $item->getItemId();
					$result[self::ORDER_ITEM_FIELD_UNIT_PRICE] = (float)$item->getBasePrice();
					if ($customOptions = $this->getCustomOptions($item)) {
						$result[self::ORDER_ITEM_FIELD_CUSTOM_OPTIONS][self::ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA] = $customOptions;
					}
					
				break;
				case 'bundle-item':
				
					$parentItem = $item->getParentItem();
					
					if ($this->isExcludedItem($parentItem)) {
						return null;
					}
										
					$shipSeparately = $this->getShipSeparately($parentItem);
					
					$result[self::ORDER_ITEM_FIELD_SKU] = $item->getSku();
					if ($shipSeparately) {
						$result[self::ORDER_ITEM_FIELD_REFERENCE] = $item->getItemId();
					}
					$result[self::ORDER_ITEM_FIELD_UNIT_PRICE] = $shipSeparately ? (float)$item->getBasePrice() : 0;
					
					if ($customOptions = $this->getCustomOptions($parentItem)) {
						$result[self::ORDER_ITEM_FIELD_CUSTOM_OPTIONS][self::ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA] = $customOptions;
					}
					
				break;
				case 'configurable-item':
				
					$parentItem = $item->getParentItem();
					
					if ($this->isExcludedItem($parentItem)) {
						return null;
					}
					
					$result[self::ORDER_ITEM_FIELD_SKU] = $parentItem->getSku();
					$result[self::ORDER_ITEM_FIELD_REFERENCE] = $parentItem->getItemId();
					$result[self::ORDER_ITEM_FIELD_UNIT_PRICE] = (float)$parentItem->getBasePrice();
					
					if ($customOptions = $this->getCustomOptions($parentItem)) {
						$result[self::ORDER_ITEM_FIELD_CUSTOM_OPTIONS][self::ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA] = $customOptions;
					}
					
				break;
				case 'simple-item':
				
					if ($this->isExcludedItem($item)) {
						return null;
					}
				
					$result[self::ORDER_ITEM_FIELD_SKU] = $item->getSku();
					$result[self::ORDER_ITEM_FIELD_REFERENCE] = $item->getItemId();
					$result[self::ORDER_ITEM_FIELD_UNIT_PRICE] = (float)$item->getBasePrice();
					
					if ($customOptions = $this->getCustomOptions($item)) {
						$result[self::ORDER_ITEM_FIELD_CUSTOM_OPTIONS][self::ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA] = $customOptions;
					}
					
				break;				
				
			}
        }
		
        return $result;
    }
	
	private function IsExcludedItem($item)
	{
		if ($item) 
		{
			if ($item->getProductType() == $this->_catalogProductType::TYPE_SIMPLE) {
				$product = $this->_catalogProduct->load($item->getProductId());
				if ($product) {
					# product attribute cg_exclude (boolean) (default false)
					return (bool)$product->getCgExclude();
				}
			}
		}
		
		return false;
	}
	
	public function getOrderRmaItems(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
	{
	    $result = [];
        foreach ($creditmemo->getAllItems() as $item)
        {
			$backToInventory = false;
			if ($item->hasBackToStock()) {
				$backToInventory = $item->getBackToStock();
			}
			
			if ($item->getQty() > 0) {
				$result[$item->getOrderItemId()] = [
					'qty' => $item->getQty(),
					'back_to_inventory' => $backToInventory
				];
			}
		}
  
		return $result;
	}

	public function getShipSeparately(\Magento\Sales\Model\Order\Item $item)
	{
		// SHIPMENT_SEPARATELY = 1;
		// SHIPMENT_TOGETHER = 0;
		
		$itemShipmentType = $item->getProductOptionByCode('shipment_type');
		
		if ($itemShipmentType != null) {
			$typeInstance = $this->_catalogProductType->factory($item->getProduct());	

			return $itemShipmentType == $typeInstance::SHIPMENT_SEPARATELY;
		}
		
		return null;
	}
	
	/**
	**/
	public function getOrderDeliveryDate(\Magento\Sales\Model\Order $order)
	{
		$field = $this->getOrderDeliveryDateField($order->getStore());
		
		if ($field) {
			return $order->getData($field);
		}
		return null;
	}
	
	/**
	**/
	public function getOrderConsignmentData(\Magento\Sales\Model\Order $order)
	{
		if ($order->getCustomerIsGuest()) {
			return null;
		}
		
		$customer = $this->_customer->load($order->getCustomerId());
		if ($consignmentZone = trim($customer->getConsignmentZone())) {
			$data = new StdClass;
			$data->zone = $consignmentZone;
			$data->type = strtolower($customer->getResource()->getAttribute('consignment_type')->getFrontend()->getValue($customer));
			return $data;
		}
		
		return null;
	}
	
    /**
     * @param Mage_Sales_Model_Order $order
     * @return int
     */
    private function getCustomOptions(\Magento\Sales\Model\Order\Item $item)
    {
		/**
			simple/grouped
				verwijder de options met een sku als deze in de options staan
			config
				verwijder de options met een sku als deze in de parent options staan
		*/

		$productOptions = $item->getProduct()->getOptions();
		$itemOptions = $item->getProductOptionByCode('options');
		$excludedOptions = $this->getExcludeCustomOptions($item->getOrder()->getStore());
		$result = null;
		
		if (is_array($itemOptions) && is_array($productOptions)) 
		{
			foreach ($itemOptions as $index => $itemOption) 
			{
				if (in_array($itemOption['label'], $excludedOptions)) {
					continue;
				}
								
				$productOptionTypeValue = $this->_catalogProductOptionValue
					->getCollection()
					->addFieldToFilter('option_type_id', $itemOption['option_value'])
					->addFieldToFilter('option_id', $itemOption['option_id'])
					->getFirstItem();
				
				$productOptionValueHasSku = false;
				if ($productOptionTypeValue) {
					$productOptionValueHasSku = $productOptionTypeValue->getSku() ? true : false;
				}	
					
				$useThisOption = true;
				foreach ($productOptions as $productOption) 
				{
					if ($itemOption['option_id'] == $productOption->getId()) {
						if ($productOption->getSku() || $productOptionValueHasSku) {
							if (!in_array($productOption->getType(), [
								$this->_catalogProductOption::OPTION_TYPE_FILE, 
								$this->_catalogProductOption::OPTION_TYPE_FIELD,
								$this->_catalogProductOption::OPTION_TYPE_AREA,
								$this->_catalogProductOption::OPTION_TYPE_DATE,
								$this->_catalogProductOption::OPTION_TYPE_TIME,
								$this->_catalogProductOption::OPTION_TYPE_DATE_TIME
							])) {
								$useThisOption = false;
							}
						}
						break;
					}
				}
				
				if ($useThisOption) {
					$result[] = [
						'label' => $itemOption['label'],
						'value' => $itemOption['value']
					];
				}
			}
		}
		
		return $result;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return float|int
     */
    private function getCODAmount(\Magento\Sales\Model\Order $order)
    {
        # check if COD payment and add COD amount field
        $paymentMethod = $order->getPayment()->getMethod();
        if (in_array($paymentMethod, $this->getCODMethods())) {
            return $order->getBaseGrandTotal();
        }
        return 0;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    private function getShippingMethod(\Magento\Sales\Model\Order $order)
    {
        $shippingMethod = $order->getShippingMethod();
		
		if (in_array($shippingMethod, $this->getPickupMethods()) || stripos($shippingMethod, self::ORDER_SHIPPING_METHOD_PICKUP) !== false) {
			return self::ORDER_SHIPPING_METHOD_PICKUP;
		}
		
        return substr($shippingMethod, 0, strpos($shippingMethod, '_'));
    }
	
    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
	public function isExcludedShippingMethod(\Magento\Sales\Model\Order $order)
	{
		return in_array($order->getShippingMethod(), $this->getExcludeShippingMethods());
	}	

    /**
     * @param Mage_Customer_Model_Address $address
     * @return array
     */
    private function getShippingAddress (\Magento\Sales\Model\Order\Address $address)
    {
        $result = [];
        if ($address) {
            $result = [
                self::ORDER_FIELD_SHIPPING_COMPANY => trim($address->getCompany()),
                self::ORDER_FIELD_SHIPPING_FIRSTNAME => trim($address->getFirstname()),
                self::ORDER_FIELD_SHIPPING_LASTNAME => trim(trim($address->getMiddlename()) . ' ' . trim($address->getLastname())),
                self::ORDER_FIELD_SHIPPING_STREET_FULL => trim($address->getStreetFull()),
                self::ORDER_FIELD_SHIPPING_POSTCODE => trim($address->getPostcode()),
                self::ORDER_FIELD_SHIPPING_CITY => trim($address->getCity()),
                self::ORDER_FIELD_SHIPPING_REGION => trim($address->getRegion()),
                self::ORDER_FIELD_SHIPPING_COUNTRY => trim($address->getCountryId()),
                self::ORDER_FIELD_SHIPPING_PHONE => str_replace(array(' ', '-', '(', ')' , '+'), array('', '', '', '', '00'), $address->getTelephone()),
                self::ORDER_FIELD_SHIPPING_EMAIL => trim($address->getEmail())
            ];
        }
        return $result;
    }

    /**
     * @param Mage_Customer_Model_Address $address
     * @return array
     */
    private function getBillingAddress (\Magento\Sales\Model\Order\Address $address)
    {
        $result = [];
        if ($address) {
            $result = [
                self::ORDER_FIELD_BILLING_COMPANY => trim($address->getCompany()),
                self::ORDER_FIELD_BILLING_FIRSTNAME => trim($address->getFirstname()),
                self::ORDER_FIELD_BILLING_LASTNAME => trim(trim($address->getMiddlename()) . ' ' . trim($address->getLastname())),
                self::ORDER_FIELD_BILLING_STREET_FULL => trim($address->getStreetFull()),
                self::ORDER_FIELD_BILLING_POSTCODE => trim($address->getPostcode()),
                self::ORDER_FIELD_BILLING_CITY => trim($address->getCity()),
                self::ORDER_FIELD_BILLING_REGION => trim($address->getRegion()),
                self::ORDER_FIELD_BILLING_COUNTRY => trim($address->getCountryId()),
                self::ORDER_FIELD_BILLING_PHONE => str_replace(array(' ', '-', '(', ')' , '+'), array('', '', '', '', '00'), $address->getTelephone()),
                self::ORDER_FIELD_BILLING_EMAIL => trim($address->getEmail()),
            ];
        }
        return $result;
    }
    
    ##################################################
    #################[ MISC METHODS ]#################
    ##################################################
    
    /**
     * create full api url
     *
     * @param $method
     * @param $store
     */
    public function getApiUrl($method, $store)
    {
        $endPoint = $this->getUrlEndPoint($store);
        return rtrim($endPoint, '/') . self::URL_API_VERSION . $method;
    }

    /**
     * get any errors from request
     *
     * @return array
     */
    public function getErrors()
    {
        return (array)$this->_errors;
    }

    /**
     * set errors from request
     *
     * @param string|array $errors
     */
    protected function setErrors($errors)
    {
        $this->_errors = $errors;
    }

    /**
     * display errors in admin
     */
    public function displayErrors()
    {
		if (!$this->isAdmin()) {
			return;
		}
		
        $errors = $this->getErrors();

        if ($errors == null) {
            return;
        }

        if (!is_array($errors)) {
            $errors = [$errors];
        }
        
        foreach ($errors as $error) {
            if (is_array($error)) {
                $error = implode(', ', $error);
            }
            $this->_messageManager->addError(__('[Clougistic] ' . $error));
        }
    }

    /**
     * display success in admin
     */
    public function displaySuccess($message)
    {
		if (!$this->isAdmin()) {
			return;
		}

        $this->_messageManager->addSuccess(__('[Clougistic] ' . $message));
    }

    /**
     * Do api request to Clougistic.
     *
     * @param $method
     * @param $data
     * @param $store
     */
    public function request($method, $data = null, $store = null)
    {
        $this->_errors = null;

        $request = json_encode([
            'userAgent' => self::USER_AGENT,
            'timestamp' => time(),
            'data' => $data
        ]);

        $apiUrl = $this->getApiUrl($method, $store);
	
        $ch = curl_init($apiUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($request),
            'X-Authorization: ' . $this->getApiKey($store),
			'X-Entity-ID: ' . $this->getEntityId($store),
            'X-Source-Host: ' . $this->getAdminHostname()
        ]);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $this->setErrors(sprintf('curl: %s [%s]', curl_error($ch), $apiUrl));
            return;
        }
        $http_status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $resultObj = json_decode($result);
        if (is_object($resultObj)) {
            if (isset($resultObj->status)) {
                $this->_debugInfo($method . ' ' . $resultObj->status);
                $this->_debugInfo($resultObj->data);
                if ($resultObj->status != 'success') {
					if (is_object($resultObj->data)) {
						foreach ($resultObj->data as $key => $value) {
							$this->setErrors(implode(', ', $value));
						}
					}
					else {
						$this->setErrors($resultObj->data . ($this->isDebug() ? "<br><br>" . implode('<br>', $resultObj->trace) . ")" : ""));
					}
                }
                return $resultObj;
            }
        }
		
        $this->setErrors(sprintf('CURL Unknown Response [status=%s]: %s', $http_status_code, $result));
    }

    /**
     * set response
     */
    public function setResponse($status, $data = null, $trace = null) {
        $this->_response->setStatus($status); 
        $this->_response->setData($data);
        $this->_response->setTrace($trace);
    }

    /**
     * get response
     */
    public function getResponse() {
        return $this->_response;
    }
    
    /**
     * debug log
     */
    protected function _debugInfo($text)
    {
		if (!is_string($text)) {
			$text = print_r($text, true);
		}
		$text = date('[Y-m-d H:i:s]') . "\r\n" . $text;
		# Mage::log($text, null, self::ERROR_LOGFILE);
    }

    /**
     * Get admin hostname
     *
     * @return mixed
     */
    public function getAdminHostname() {
        $adminUrl = $this->_backendUrl->getBaseUrl();
        $urlParts = parse_url($adminUrl);
        return @$urlParts['host'];
    }
}