<?php namespace Clougistic\Connector\Observer;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
class SalesOrderSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    protected $_api;
    protected $_apiHelper;
    
    public function __construct(
        \Clougistic\Connector\Model\Api $api,
        \Clougistic\Connector\Helper\Api $apiHelper
    ) {
        $this->_api = $api;
        $this->_apiHelper = $apiHelper;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {	
		$order = $observer->getEvent()->getOrder();
		
        # todo port to magento 2
		#if (Mage::helper('core')->isModuleEnabled('Bc_Deliverydate')) {
		#	if ($this->_apiHelper->isAdmin()) {
		#		Mage::helper('deliverydate')->saveShippingArrivalDateAdmin($observer);
		#	} else {
		#		Mage::helper('deliverydate')->saveShippingArrivalDate($observer);
		#	}
		#}
		
        $this->_api->updateOrder($order);
	}
}