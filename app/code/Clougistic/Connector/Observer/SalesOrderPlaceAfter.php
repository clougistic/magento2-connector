<?php namespace Clougistic\Connector\Observer;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
class SalesOrderPlaceAfter implements \Magento\Framework\Event\ObserverInterface
{
    protected $api;
    
    public function __construct(
        \Clougistic\Connector\Model\Api $api
    ) {
        $this->_api = $api;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {	
        $order = $observer->getEvent()->getOrder();
        $this->_api->placeOrder($order); 
	}
}