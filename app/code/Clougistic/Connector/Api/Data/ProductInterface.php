<?php namespace Clougistic\Connector\Api\Data;
 
interface ProductInterface
{
    /**
     * @api
     * @return string
     */
    public function getSku();
    
    /**
     * @api
     * @param string $sku
     */
    public function setSku($sku);

    /**
     * @api
     * @return float
     */
    public function getQty();

    /**
     * @api
     * @param float $qty
     */
    public function setQty($qty);
    
    /**
     * @api
     * @return string
     */
    public function getEta();
    
    /**
     * @api
     * @param string $eta
     */
    public function setEta($eta);
    
    /**
     * @api
     * @return string
     */
    public function getEtaField();
    
    /**
     * @api
     * @param string $eta_field
     */
    public function setEtaField($eta_field);
    
    /**
     * @api
     * @return float
     */
    public function getCost();
    
    /**
     * @api
     * @param float $cost
     */
    public function setCost($cost);
}