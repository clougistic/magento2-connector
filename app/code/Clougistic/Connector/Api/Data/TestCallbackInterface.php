<?php namespace Clougistic\Connector\Api\Data;
 
interface TestCallbackInterface
{
    /**
     * @api
     * @return string
     */
    public function getHost();

    /**
     * @api
     * @param string $host
     * @return void
     */
    public function setHost($host);

    /**
     * @api
     * @param \Clougistic\Connector\Api\Data\TestCallbackInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
    public function testCallback($data);
}