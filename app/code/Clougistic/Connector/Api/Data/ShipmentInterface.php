<?php namespace Clougistic\Connector\Api\Data;
 
interface ShipmentInterface
{
    /**
     * @api
     * @return string
     */
    public function getOrderReference();
    
    /**
     * @api
     * @param string $reference
     */
    public function setOrderReference($reference);

    /**
     * @api
     * @return int
     */
    public function getShipmentId();

    /**
     * @api
     * @param int $id
     */
    public function setShipmentId($id);
    
    /**
     * @api
     * @return string
     */
    public function getCarrierCode();
    
    /**
     * @api
     * @param string $code
     */
    public function setCarrierCode($code);
    
    /**
     * @api
     * @return string
     */
    public function getCarrierOptions();
    
    /**
     * @api
     * @param string $options
     */
    public function setCarrierOptions($options);
    
    /**
     * @api
     * @return int
     */
    public function getParcelCount();
    
    /**
     * @api
     * @param int $qty
     */
    public function setParcelCount($qty);
    
    /**
     * @api
     * @return array
     */
    public function getPrintNode();
    
    /**
     * @api
     * @param array $printNode
     */
    public function setPrintNode($printNode);
    
    /**
     * @api
     * @return array
     */
    public function getItems();
    
    /**
     * @api
     * @param array $items
     */
    public function setItems($items);
        
}