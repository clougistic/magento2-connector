<?php namespace Clougistic\Connector\Api\Data;
 
interface OrderItemInterface
{
    /**
     * @api
     * @return string
     */
    public function getReference();
    
    /**
     * @api
     * @param string $sku
     */
    public function setReference($reference);

    /**
     * @api
     * @return float
     */
    public function getCost();
    
    /**
     * @api
     * @param float $cost
     */
    public function setCost($cost);
}