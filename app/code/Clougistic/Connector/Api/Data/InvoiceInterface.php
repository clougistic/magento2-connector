<?php namespace Clougistic\Connector\Api\Data;
 
interface InvoiceInterface
{
    /**
     * @api
     * @return string
     */
    public function getOrderReference();
    
    /**
     * @api
     * @param string $reference
     */
    public function setOrderReference($reference);
    
    /**
     * @api
     * @return array
     */
    public function getItems();
    
    /**
     * @api
     * @param array $items
     */
    public function setItems($items);
        
}