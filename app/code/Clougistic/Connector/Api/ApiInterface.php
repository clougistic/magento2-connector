<?php namespace Clougistic\Connector\Api;
 
interface ApiInterface
{
    /**
     * @api
     * @param \Clougistic\Connector\Api\Data\ProductInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
    public function updateProduct($data);

    /**
     * @api
     * @param \Clougistic\Connector\Api\Data\ShipmentInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
    public function createShipment($data);

    /**
     * @api
     * @param \Clougistic\Connector\Api\Data\InvoiceInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
    public function createAndPrintInvoice($data);

    /**
     * @api
     * @param \Clougistic\Connector\Api\Data\ShipmentInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
    public function printShipmentLabels($data);

    /**
     * @api
     * @param \Clougistic\Connector\Api\Data\OrderItemInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
    public function updateOrderItemCost($data);    
}