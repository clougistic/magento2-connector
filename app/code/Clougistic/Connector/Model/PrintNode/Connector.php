<?php namespace Clougistic\Connector\Model\PrintNode;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_PrintNode_Connector
 *
 */
 
use Magento\Framework\Webapi\Exception;

class Connector
{
    /**
     * Interface url. http or https calculate by store protocol
     * @var string
     */
    const API_URL = 'https://api.printnode.com';

	private $_apiKey = null;
	
	/**
	 * Set Api Key
	 */
	public function setApiKey($apiKey)
	{
		$this->_apiKey = $apiKey;
	}
	
    /**
     * Submit document to printer
     *
     * @param string $document Document
     * @param string $type Type
     * @param string $title Title
     * @return bool
     */
    public function submit($document, $printerId, $title = 'Print Job', $type = 'pdf_base64')
    {
        if (!$document) {
			return false;
		}
        if (!$printerId) {
			return false;
		}

        $result = $this->request('printjobs', [
            'printer' => $printerId, 
            'title' => $title, 
            'content' => $document, 
            'contentType' => $type, 
            'source' => 'Clougistic Connector Magento'
        ]);

        return $result;
    }

    /**
     * Return Printers list
     *
     * @return array
     */
    public function search()
	{
        $result = json_decode($this->request('printers'));
        if ($result) {
            return $result;
        }
        return false;
    }
    
    private function request($method, $data = null)
    {
        if (strlen($this->_apiKey) == 0) {
            throw new Exception(__('No PrintNode api key set.'));
        }
        
        $url = self::API_URL . '/' . $method;
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $this->_apiKey,
        ]);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception(__('[PrintNode] ' . curl_error($ch)));
            return;
        }
        $http_status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $result;        
    }
}