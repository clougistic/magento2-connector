<?php namespace Clougistic\Connector\Model\System\Config\Source\Consignment;

class Type extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
    
    public function getAllOptions()
    {
        return [
            ['value' => 'sale',   'label' => __('Sale')],
            ['value' => 'supply', 'label' => __('Supply')]
        ];
    }
}