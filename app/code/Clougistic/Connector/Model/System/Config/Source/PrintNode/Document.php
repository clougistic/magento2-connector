<?php namespace Clougistic\Connector\Model\System\Config\Source\PrintNode;

class Document extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource implements \Magento\Framework\Option\ArrayInterface
{
    protected $_apiHelper = null;
    
    public function __construct (\Clougistic\Connector\Helper\Api $apiHelper)
    {
        $this->_apiHelper = $apiHelper;
    }

    public function toOptionArray($dummy = null, $sort = SORT_ASC)
    {
        return $this->getAllOptions($dummy, $sort);
    }
    
    public function getAllOptions($dummy = null, $sort = SORT_ASC)
    {
        $documents = $this->_apiHelper->getCarrierSelector()->getAllDocuments();

        foreach ($documents as $key => $row) {
			$label[$key] = $row['label'];
		}

		array_multisort ($label, $sort, $documents);
	
        return $documents;
    }
}