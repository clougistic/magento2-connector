<?php namespace Clougistic\Connector\Model\System\Config\Source\Payment;

class Method extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource implements \Magento\Framework\Option\ArrayInterface
{
    protected $_paymentHelper = null;
    
    public function __construct (\Magento\Payment\Helper\Data $paymentHelper)
    {
        $this->_paymentHelper = $paymentHelper;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
    
    public function getAllOptions()
    {
        return $this->_paymentHelper->getPaymentMethodList(true, true, true);
    }
}