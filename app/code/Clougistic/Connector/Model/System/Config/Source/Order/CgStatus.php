<?php namespace Clougistic\Connector\Model\System\Config\Source\Order;

class CgStatus extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource implements \Magento\Framework\Option\ArrayInterface
{
    protected $_apiHelper;
    
    public function __construct(
        \Clougistic\Connector\Helper\Api $apiHelper
    ) {
        $this->_apiHelper = $apiHelper;
    }
    
    public function toOptionArray()
    {
        return [
            $this->_apiHelper::ORDER_UNSYNCED => __('Unsynced'), 
			$this->_apiHelper::ORDER_SYNCED => __('Synced'), 
			$this->_apiHelper::ORDER_INVALID => __('Invalid'),
			$this->_apiHelper::ORDER_ERROR => __('Error')
        ];   
    }
    
    public function getAllOptions()
    {
        return [
            ['value' => $this->_apiHelper::ORDER_UNSYNCED,  'label' => __('Unsynced')], 
			['value' => $this->_apiHelper::ORDER_SYNCED,    'label' => __('Synced')], 
			['value' => $this->_apiHelper::ORDER_INVALID,   'label' => __('Invalid')],
			['value' => $this->_apiHelper::ORDER_ERROR,     'label' => __('Error')]
        ];
    }
}