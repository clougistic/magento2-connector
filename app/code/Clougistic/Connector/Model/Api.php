<?php namespace Clougistic\Connector\Model;

use Clougistic\Connector\Api\ApiInterface;
use Clougistic\Connector\Helper\Api as ApiHelper;
use Magento\Framework\DB\Transaction;
use Magento\Framework\Registry;
use Magento\Framework\Webapi\Exception;

class Api implements ApiInterface
{
    const GREETINGS_METHOD = '/greetings';
    const GREETINGS_METHOD_EXPECTED_RESPONSE = "You're welcome!";

    const TEST_CLIENT_CALLBACK_METHOD = '/test-client-callback';
    const TEST_CLIENT_CALLBACK_MESSAGE = 'Passed: I called myself!';

    const ORDER_METHOD_SAVE = '/order/save';
    const ORDER_METHOD_UPDATE = '/order/update';
	const ORDER_METHOD_HOLD = '/order/hold';
	const ORDER_METHOD_UNHOLD = '/order/unhold';
	const ORDER_METHOD_CANCEL = '/order/cancel';
    const ORDER_METHOD_CREATE_RMA = '/order/create-rma';
    
	const PRODUCT_METHOD_SYNC = '/product/sync';
	const PRODUCT_METHOD_SYNC_STATUS_CREATED = 'created';
	const PRODUCT_METHOD_SYNC_STATUS_SKIPPED = 'skipped';
	const PRODUCT_METHOD_SYNC_STATUS_ERROR = 'error';

	const PRODUCT_REPLENISHMENT_POLICY_NONE = 0;
	const PRODUCT_REPLENISHMENT_POLICY_MINMAX = 1;
    
    const PRODUCT_ATTRIBUTE_COST = 'cost';
    const ORDER_ITEM_ATTRIBUTE_CG_COST = 'cg_cost';
	
	const CLOUGISTIC_CONNECTOR_FLAG_PLACE_ORDER = 'clougistic_connector_flag_place_order';
	const CLOUGISTIC_CONNECTOR_FLAG_UPDATE_ORDER = 'clougistic_connector_flag_update_order';
	const CLOUGISTIC_CONNECTOR_FLAG_SAVE_ORDER_RMA = 'clougistic_connector_flag_save_order_rma';
    
    const RESPONSE_STATUS_SUCCESS = 'success';
    const RESPONSE_STATUS_ERROR = 'error';
    const RESPONSE_STATUS_ERROR_GRACEFUL = 'error-graceful';
    
    protected $_apiHelper;
    protected $_transaction;
    protected $_registry;
    
    public function __construct(
        ApiHelper $apiHelper,
        Transaction $transaction,
        Registry $registry
    ) {
        $this->_apiHelper = $apiHelper;
        $this->_transaction = $transaction;
        $this->_registry = $registry;
    }

    #################[ LOCAL API METHODS ]##################
    /**
     * Sending methods
     */
    
    /**
     * Send greetings to Clougistic to check the connection state
     */
    public function greetings(&$systemInfo)
    {
		if (!$this->_apiHelper->isEnabled()) {
			return false;
		}

		$systemInfo = null;
			
        $result = $this->_apiHelper->request(self::GREETINGS_METHOD);
        if (@$result->status == self::RESPONSE_STATUS_SUCCESS) {
            $systemInfo = (object)$result->data->system_info;
            return $result->data->answer == self::GREETINGS_METHOD_EXPECTED_RESPONSE;
        }
        
        return false;
    }

    /**
     * @return mixed
     */
    public function testCallback()
    {
        $result = $this->_apiHelper->request(self::TEST_CLIENT_CALLBACK_METHOD);

        if (isset($result->status)) {
            return __FUNCTION__ . ': ' . $result->data;
        }

        return __FUNCTION__ . ': invalid response. check your connector settings.';
    }


    /**
     * Place Order in Clougistic
     *
     * @param \Magento\Sales\Model\Order $order
     */
    public function placeOrder (\Magento\Sales\Model\Order $order)
    {
		# catch recursion
		if ($this->_registry->registry(self::CLOUGISTIC_CONNECTOR_FLAG_PLACE_ORDER)) {
            return;
        }
		
        $store = $order->getStore();
        
		# is connector enabled?
		if (!$this->apiHelper->isEnabled($store)) {
            return;
        }

		# do not sync excluded shipping methods
		if ($this->_apiHelper->isExcludedShippingMethod($order)) {
			return;
		}
		
		# if the cg status is unsynced, it's a new order in clougistic
		if ($order->getCgStatus() != $this->_apiHelper::ORDER_UNSYNCED) {
			return;
		}
		
		$paymentMethod = strtolower($order->getPayment()->getMethodInstance()->getCode());

		# MultiSafePay (msp) check if order has been paid
		if (stripos($paymentMethod, 'msp_') !== false && $paymentMethod != 'msp_banktransfer') {
			if ($order->getBaseTotalDue() > 0) {
				return;
			}
		}		
		
		# Klarna check if order has been paid
		if (stripos($paymentMethod, 'klarna_') !== false) {
			if ($order->getBaseTotalDue() > 0) {
				return;
			}
		}
		
		$this->_registry->register(self::CLOUGISTIC_CONNECTOR_FLAG_PLACE_ORDER, true);

		$data = $this->_apiHelper->prepareSaveOrder($order);
		
		if ($this->_apiHelper->isValidData($data)) {
			$result = $this->_apiHelper->request(self::ORDER_METHOD_SAVE, $data, store);
			
			switch (@$result->status) {
			case 'success':
				$order->setCgStatus($this->_apiHelper::ORDER_SYNCED);
				$order->addStatusHistoryComment('Order synced with Clougistic.');
				$order->save();
				$this->displaySuccess('Order synced with Clougistic.');
            break;
			default:
				$order->addStatusHistoryComment(sprintf('Order can not be synced with Clougistic. [status=%s]', @$result->status));
				$order->save();
				$this->displayErrors();
            break;
			}
		}
		else {
			$order->setCgStatus($this->_apiHelper::ORDER_INVALID);
			$order->addStatusHistoryComment('Order contains invalid or missing data and can not be synced with Clougistic.');
			$order->save();

			$this->setErrors('Order contains invalid or missing data and can not be synced with Clougistic.');
			$this->displayErrors();
		}
	
		$this->_registry->unregister(self::CLOUGISTIC_CONNECTOR_FLAG_PLACE_ORDER);
    }
	
	/**
     * Update Order in Clougistic
     *
     * @param \Magento\Sales\Model\Order $order
     * @param bool $manual
     */
    public function updateOrder (\Magento\Sales\Model\Order $order, $manual = false)
    {
		# catch recursion
		if ($this->_registry->registry(self::CLOUGISTIC_CONNECTOR_FLAG_UPDATE_ORDER)) {
            return;
        }
		
		# try placing order if it's not synced yet
		if ($order->getCgStatus() != $this->_apiHelper::ORDER_SYNCED) {
			$this->placeOrder($order);
			return;
		}
		
        $store = $order->getStore();
        
		# is connector enabled?
		if (!$this->_apiHelper->isEnabled($store)) {
            return;
        }

		# only if there is a state change, update clougistic order
		if (!$manual) {
			if ($order->getOrigData('state') == $order->getState()) {
				return;
			}
		}

		# don't update POS orders
		$posEnabled = $this->_apiHelper->isPOSEnabled($store);
		if ($posEnabled && $this->_apiHelper->isPOSOrder($order)) {
			return;
		}
	
		$this->_registry->register(self::CLOUGISTIC_CONNECTOR_FLAG_UPDATE_ORDER, true);
	
		$result = $this->_apiHelper->request(self::ORDER_METHOD_UPDATE, $this->_apiHelper->prepareUpdateOrder($order), $store);

		switch (@$result->status) {
		case 'success':
			$order->addStatusHistoryComment('Order has been updated in Clougistic.');
			$order->save();
			$this->displaySuccess('Order has been updated in Clougistic.');
        break;
		default:
			$order->addStatusHistoryComment(sprintf('Order can not be updated in Clougistic. [status=%s]', @$result->status));
			$order->save();
			$this->displayErrors();
        break;
		}

		$this->_registry->unregister(self::CLOUGISTIC_CONNECTOR_FLAG_UPDATE_ORDER);
    }
		
	/**
     * Create RMA in Clougistic
     *
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
	 * @param boolean $allItems
     */
	public function saveOrderRma (\Magento\Sales\Model\Order\Creditmemo $creditmemo, $allItems = false)
	{
		# catch recursion
		if ($this->_registry->registry(self::CLOUGISTIC_CONNECTOR_FLAG_SAVE_ORDER_RMA)) {
            return;
        }

		$store = $creditmemo->getStore();
		
		if (!$this->_apiHelper->isEnabled($store)) {
            return;
        } 

		$this->_registry->register(self::CLOUGISTIC_CONNECTOR_FLAG_SAVE_ORDER_RMA, true);
		
        $data = $this->_apiHelper->prepareSaveOrderRma($creditmemo, $allItems);

		if ($this->_apiHelper->isValidData($data)) {
			$result = $this->_apiHelper->request(self::ORDER_METHOD_CREATE_RMA, $data, $store);

			switch (@$result->status) {
			case 'success':
				$creditmemo->addComment('RMA has been created in Clougistic.', false, false);
				$creditmemo->save();
				$this->displaySuccess('RMA has been created in Clougistic.');
			break;	
			default:
				$creditmemo->addComment(sprintf('RMA can not be created in Clougistic. [status=%s]', @$result->status), false, false);
				$creditmemo->save();
				$this->displayErrors();
			break;
			}
		}
		
		$this->_registry->unregister(self::CLOUGISTIC_CONNECTOR_FLAG_SAVE_ORDER_RMA);
	}
	
    #################[ REMOTE API METHODS ]##################
    /**
     * Receiving methods
     */
    
    /**
     * Update product from clougistic
     *
     * @api
     * @param \Clougistic\Connector\Api\Data\ProductInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */ 
    public function updateProduct($data) 
    {
        try {
            
            if (!$this->_apiHelper->isEnabled()) {
                throw new Exception(__('Clougistic Connector Module is not enabled.'));
            }
                    
            $product = $data->getProduct();
            if ($product) 
            {
                $productResource = $product->getResource();
                $costUpdate = false;
                $etaUpdate = false;
                
                if ($data->getCost() !== null) {
                    $costUpdate = $product->getAttribute(self::PRODUCT_ATTRIBUTE_COST);
                }
                
                if ($data->getEtaField() !== null) {
                    $etaUpdate = $product->getAttribute($data->getEtaField());
                }
                
                if ($etaUpdate) {
                    $product->setData($data->getEtaField(), $data->getEta());
                    $productResource->saveAttribute($product, $data->getEtaField());
                }
                
                if ($costUpdate) {
                    $product->setCost($data->getCost());
                    $productResource->saveAttribute($product, self::PRODUCT_ATTRIBUTE_COST);
                }
                
                # stock qty
                if ($data->getQty() !== null) {
                    $stockItem = $data->getStockItem();
                    if ($stockItem) {
                        $stockItem->setQty($data->getQty());
                        if ($data->getQty() > 0) {
                            $stockItem->setIsInStock(1);
                            
                            # set eta to null if there is stock
                            if ($etaUpdate) {
                                $product->setData($data->getEtaField(), null);
                                $productResource->saveAttribute($product, $data->getEtaField());
                            }
                        }
                        $stockItem->save();
                        
                        // todo Magento 2 FPC flush product
                        // Lesti FPC - Invalidate cache for this product
                        #if (Mage::helper('core')->isModuleEnabled('Lesti_Fpc')) {
                        #    $fpc = Mage::getSingleton('fpc/fpc');
                        #    if ($fpc) {
                        #        $fpc->clean(sha1('product_' . $product->getId()));
                        #    }
                        #}
                        
                        $this->_apiHelper->setResponse(self::RESPONSE_STATUS_SUCCESS);
                    }
                    else {
                        $this->_apiHelper->setResponse(self::RESPONSE_STATUS_ERROR_GRACEFUL, __(sprintf('Stock Item for Product with SKU [%s] not found.', $data->getSku())));
                    }
                }
            } else {
                $this->_apiHelper->setResponse(self::RESPONSE_STATUS_ERROR_GRACEFUL, __(sprintf('Product with SKU [%s] not found.', $data->getSku())));
            }
            
        } catch (Exception $e) {
            $this->_apiHelper->setResponse(self::RESPONSE_STATUS_ERROR, __($e->getMessage()), $e->getTraceAsString());
        }
        
        return $this->_apiHelper->getResponse();
    }
    
    /**
     * Create shipment from Clougistic
     *
     * @api
     * @param \Clougistic\Connector\Api\Data\ShipmentInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
    public function createShipment($data)
    {
		try
        {
			if ($data->getShipmentId() === null) {
				throw new Exception(__('Missing Shipment ID for creating Shipment.'));
			}
			
			if ($data->getOrderReference() === null) {
				throw new Exception(__('Missing Order reference for creating Shipment.'));
			}
			
			if ($data->getItems() === null) {
				throw new Exception(__('Missing Items for creating Shipment.'));
			}

			if (count($data->getItems()) == 0) {
				throw new Exception(__('No items to ship.'));
			}

			$order = $data->getOrder();
			if (!$order) {
				throw new Exception(__(sprintf('Order Reference [%s] not found.', $data->getOrderReference())));
			}

            if (!$this->_apiHelper->isEnabled($order->getStore())) {
                throw new Exception(__('Clougistic Connector Module is not enabled.'));
            }

            if (!$order->canShip()) {
                throw new Exception(__(sprintf('Order [%s] can not be shipped.', $data->getOrderReference())));
            }
			
			$carrierSelector = $this->_apiHelper->getCarrierSelector();
			
            $shipment = $data->getOrderToShipment();
            $shipment->increment_id = sprintf('%s%s', $this->_apiHelper->getShipmentIncrementIdPrefix($order->getStore()), $data->getShipmentId());
            
            foreach ($data->getItems() as $item) {
                $foundItem = false;
                foreach ($order->getAllItems() as $orderItem) {
                    if ($orderItem->getItemId() == $item['id']) {
                        $shipableQty = $orderItem->getQtyToShip();
                        if ($shipableQty < $item['qty']) {
                            throw new Exception(__(sprintf('Item [ref=%s, sku=%s] qty [%s] exceeds shipable qty [%s].', $orderItem->getItemId(), $orderItem->getSku(), $item['qty'], $shipableQty)));
                        }
                        $foundItem = true;
                        break;
                    }					
                }
                
                if (!$foundItem) {
                    throw new Exception(__(sprintf('Order Item with ID [%s] not found for Order [%s].', $item['id'], $order->getIncrementId())));
                }
                
                $shipmentItem = $data->getOrderItemToShipmentItem($orderItem, $item['qty']);
                $shipment->addItem($shipmentItem);
            }
                            
            $shipment->register();
            $order->setIsInProcess(true);
            $order->setCustomerNoteNotify(false);

            $carrierSelector->before($shipment, $data);

            $this->_registry->register(self::CLOUGISTIC_CONNECTOR_FLAG_UPDATE_ORDER, true);
            
            $this->_transaction->addObject($shipment);
            $this->_transaction->addObject($order);
            $this->_transaction->save();
            
            if (!$shipment->getData('email_sent')) {
                $shipment->sendEmail(true);
                $shipment->setEmailSent(true);
                $shipment->save();
            }

        	$this->_apiHelper->setResponse(self::RESPONSE_STATUS_SUCCESS);
        }
		catch (Exception $e) {
            $this->_apiHelper->setResponse(self::RESPONSE_STATUS_ERROR, __($e->getMessage()), $e->getTraceAsString());
        }
        
        $this->_registry->unregister(self::CLOUGISTIC_CONNECTOR_FLAG_UPDATE_ORDER);

        return $this->_apiHelper->getResponse();
    }
    
    /**
     * Create and print Invoice from Clougistic
     *
     * @param \Clougistic\Connector\Api\Data\InvoiceInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
	public function createAndPrintInvoice($data)
	{
		try 
		{
			if ($data->getItems() === null) {
				throw new Exception(__('Missing Items for creating Invoice.'));
			}

			if (count($data->getItems()) == 0) {
				throw new Exception(__('No items to Invoice.'));
			}
			
            $order = $data->getOrder();
			if (!$order) {
				throw new Exception(__(sprintf('Order Reference [%s] not found.', $data->getOrderReference())));
			}

            if (!$this->_apiHelper->isEnabled($order->getStore())) {
                throw new Exception(__('Clougistic Connector Module is not enabled.'));
            }

            if (!$order->canInvoice()) {
				throw new Exception(__(sprintf('Order Reference [%s] can not be invoiced.', $data->getOrderReference())));
			}
            
			$carrierSelector = $this->_apiHelper->getCarrierSelector();
		
            $invoice = $data->getOrderToInvoice();
			
			foreach ($data->getItems() as $item) {
                $foundItem = false;
				foreach ($order->getAllItems() as $orderItem) {
					if ($orderItem->getItemId() == $item['id']) {
						$invoicableQty = $orderItem->getQtyShipped() - $orderItem->getQtyInvoiced();
						if ($invoicableQty > 0) {
                            $invoiceItem = $data->getOrderItemToInvoiceItem($orderItem, $item['qty']);
                            $invoice->addItem($invoiceItem);
                            $foundItem = true;
						}
						break;
					}					
				}
                
                if (!$foundItem) {
                    throw new Exception(__(sprintf('Order Item with ID [%s] not found for Order [%s].', $item['id'], $order->getIncrementId())));
                }
			}

            $invoice->register();
            $order->setIsInProcess(true);

			if ($invoice->canCapture()) {
				if ($order->getPayment()->getMethodInstance()->isGateway()) {
					$invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
				} else {
					$invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
				}
			} else {
				$invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::NOT_CAPTURE);
			}

            $this->_registry->register(self::CLOUGISTIC_CONNECTOR_FLAG_UPDATE_ORDER, true);

            $this->_transaction->addObject($invoice);
            $this->_transaction->addObject($order);
            $this->_transaction->save();
			
            if (!$invoice->getData('email_sent')) {
                $invoice->sendEmail(true);
            }

            $carrierSelector->afterInvoice($invoice, $data);

			$this->_apiHelper->setResponse(self::RESPONSE_STATUS_SUCCESS);
		}
		catch (Exception $e) {
            $this->_apiHelper->setResponse(self::RESPONSE_STATUS_ERROR, __($e->getMessage()), $e->getTraceAsString());
        }

        $this->_registry->unregister(self::CLOUGISTIC_CONNECTOR_FLAG_UPDATE_ORDER);
		
        return $this->_apiHelper->getResponse();
	}
	
	/**
     * Print shipment labels from Clougistic.
     *
     * @param \Clougistic\Connector\Api\Data\ShipmentInterface $data
     * @return \Clougistc\Connector\Model\Api\Response
     */
	public function printShipmentLabels($data)
	{
		try 
		{
			if ($data->getShipmentId() === null) {
				throw new Exception(__('Missing Shipment ID for printing Shipment labels.'));
			}
			
			$order = $data->getOrder();
			if (!$order) {
				throw new Exception(__(sprintf('Order Reference [%s] not found.', $data->order_reference)));
			}
			
			if (!$this->_apiHelper->isEnabled($order->getStore())) {
				throw new Exception(__('Clougistic Connector Module is not enabled.'));
			} 
			
			$carrierSelector = $this->_apiHelper->getCarrierSelector();
			
			$incrementId = sprintf('%s%s', $this->_apiHelper->getShipmentIncrementIdPrefix($order->getStore()), $data->getShipmentId());
			$shipment = $data->loadShipment($incrementId);
			
			if (!$shipment) {
				throw new Exception(__(sprintf('Shipment [%s] not found.', $incrementId)));
			}

			$carrierSelector->afterShipment($shipment, $data);
			
            $this->_apiHelper->setResponse(self::RESPONSE_STATUS_SUCCESS);
		}
		catch (Exception $e) {
            $this->_apiHelper->setResponse(self::RESPONSE_STATUS_ERROR, __($e->getMessage()), $e->getTraceAsString());
		}
		
		return $this->_apiHelper->getResponse();
	}

	/**
     * Update Order Item Cost from Clougistic.
     *
     * @param \Clougistic\Connector\Api\Data\OrderItemInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
	public function updateOrderItemCost($data)
	{
		try 
		{
			if ($data->getReference() === null) {
				throw new Exception(__('Missing reference.'));
			}

			if ($data->getCost() === null) {
				throw new Exception(__('Missing cost.'));
			}
			
			$orderItem = $data->getOrderItem();
			if (!$orderItem) {
				throw new Exception(__(sprintf('Order Item with Reference [%s] not found.', $data->getReference())));
			}
			
			$orderItem->setData(self::ORDER_ITEM_ATTRIBUTE_CG_COST, $data->getCost());
			$orderItem->getResource()->saveAttribute($orderItem, self::ORDER_ITEM_ATTRIBUTE_CG_COST);

            $this->_apiHelper->setResponse(self::RESPONSE_STATUS_SUCCESS);
		}
		catch (Exception $e) {
            $this->_apiHelper->setResponse(self::RESPONSE_STATUS_ERROR, __($e->getMessage()), $e->getTraceAsString());
		}
		
		return $this->_apiHelper->getResponse();
	}
}