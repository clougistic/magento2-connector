<?php namespace Clougistic\Connector\Model\Api\Data;

use Clougistic\Connector\Api\Data\ShipmentInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Convert\Order as OrderConverter;

class Shipment implements ShipmentInterface
{
    private $order_reference;
    private $shipment_id;
    private $carrier_code;
    private $carrier_options;
    private $parcel_count;
    private $printnode = [];
    private $items = [];
    
    protected $_order;
    protected $_orderFactory;
    protected $_shipmentFactory;
    protected $_orderConverter;
    
    public function __construct (
        OrderFactory $orderFactory,
        ShipmentFactory $shipmentFactory,
        OrderConverter $orderConverter
    ) {
        $this->_orderFactory = $orderFactory->create();
        $this->_shipmentFactory = $shipmentFactory->create();
        $this->_orderConverter = $orderConverter->create();
    }
    
    /**
     * @api
     * @return string
     */
    public function getOrderReference() {
        return $this->order_reference;
    }
    
    /**
     * @api
     * @param string $sku
     */
    public function setOrderReference($reference) {
        $this->order_reference = $reference;
        $this->_order = $this->_orderFactory->loadByIncrementId($this->order_reference);
    }

    /**
     * @api
     * @return int
     */
    public function getShipmentId() {
        return $this->shipment_id;
    }

    /**
     * @api
     * @param int $id
     */
    public function setShipmentId($id) {
        $this->shipment_id = $id;
    }
    
    /**
     * @api
     * @return string
     */
    public function getCarrierCode() {
        return $this->carrier_code;
    }
    
    /**
     * @api
     * @param string $code
     */
    public function setCarrierCode($code) {
        $this->carrier_code = $code;
    }

    /**
     * @api
     * @return string
     */
    public function getCarrierOptions() {
        return $this->carrier_options;
    }
    
    /**
     * @api
     * @param string $options
     */
    public function setCarrierOptions($options) {
        $this->carrier_options = $options;
    }

    /**
     * @api
     * @return int
     */
    public function getParcelCount() {
        return $this->parcel_count;
    }
    
    /**
     * @api
     * @param int $qty
     */
    public function setParcelCount($qty) {
        $this->parcel_count = (int)$qty;
    }

    /**
     * @api
     * @return array
     */
    public function getPrintNode() {
        return $this->printnode;
    }

    /**
     * @api
     * @param array $printNode
     */
    public function setPrintNode($printNode) {
        $this->printnode = $printNode;
    }

    /**
     * @api
     * @return array
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @api
     * @param array $items
     */
    public function setItems($items) {
        $this->items = $items;
    }
    
    /**
     * get order with order reference
     */
    public function getOrder() {
        return $this->_order;
    }
    
    /**
     * create a shipment from the order
     */
    public function getOrderToShipment() {
        return $this->_orderConverter->toShipment($this->_order);
    }
    
    /**
     * create a shipment item from the order item
     *
     * @param $orderItem order item to ship
     * @param $qty qty shipped
     */
    public function getOrderItemToShipmentItem($orderItem, $qty) {
        return $this->_orderConverter->itemToShipmentItem($orderItem)->setQty($qty);
    }
    
    /**
     * get shipment by increment id
     *
     * @param $orderItem order item to ship
     * @param $qty qty shipped
     */
    public function loadShipment($incrementId) {
        return $this->_shipmentFactory->loadByIncrementId($incrementId);
    }
}