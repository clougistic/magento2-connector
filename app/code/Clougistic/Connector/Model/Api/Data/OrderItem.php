<?php namespace Clougistic\Connector\Model\Api\Data;

use Clougistic\Connector\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order\ItemFactory as OrderItemFactory;

class OrderItem implements OrderItemInterface
{
    private $reference;
    private $cost;
    
    protected $_orderItem;
    protected $_orderItemFactory;
    
    public function __construct (
        OrderItemFactory $orderItemFactory
    ) {
        $this->_orderItemFactory = $orderItemFactory->create();
    }
    
    /**
     * @api
     * @return string
     */
    public function getReference() {
        return $this->reference;
    }
    
    /**
     * @api
     * @param string $sku
     */
    public function setReference($reference) {
        $this->reference = $reference;
        $this->_orderItem = $this->_orderItemFactory->load($this->reference);
    }

    /**
     * @api
     * @return float
     */
    public function getCost() {
        return $this->cost;
    }
    
    /**
     * @api
     * @param float $cost
     */
    public function setCost($cost) {
        $this->cost = (float)$cost;
    }
    
    /**
     * get order item with reference (id)
     */
    public function getOrderItem() {
        return $this->_orderItem;
    }
}