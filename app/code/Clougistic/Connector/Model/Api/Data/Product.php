<?php namespace Clougistic\Connector\Model\Api\Data;

use Clougistic\Connector\Api\Data\ProductInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;

class Product implements ProductInterface
{
    private $sku;
    private $qty;
    private $eta;
    private $eta_field;
    private $cost;
    
    protected $_product;
    protected $_productFactory;
    protected $_stockItemRepository;
    
    public function __construct (
        ProductFactory $productFactory,
        StockItemRepository $stockItemRepository
    ) {
        $this->_productFactory = $productFactory->create();
        $this->_stockItemRepository = $stockItemRepository;
    }
    
    /**
     * @api
     * @return string
     */
    public function getSku() {
        return $this->sku;
    }
    
    /**
     * @api
     * @param string $sku
     */
    public function setSku($sku) {
        $this->sku = $sku;
        $this->_product = $this->_productFactory->loadByAttribute('sku', $this->sku);
    }

    /**
     * @api
     * @return float
     */
    public function getQty() {
        return $this->qty;
    }

    /**
     * @api
     * @param float $qty
     */
    public function setQty($qty) {
        $this->qty = (float)$qty;
    }
    
    /**
     * @api
     * @return string
     */
    public function getEta() {
        return $this->eta;
    }
    
    /**
     * @api
     * @param string $eta
     */
    public function setEta($eta) {
        $this->eta = $eta;
    }

    /**
     * @api
     * @return string
     */
    public function getEtaField() {
        return $this->eta_field;
    }
    
    /**
     * @api
     * @param string $eta_field
     */
    public function setEtaField($eta_field) {
        $this->eta_field = $eta_field;
    }

    /**
     * @api
     * @return float
     */
    public function getCost() {
        return $this->cost;
    }
    
    /**
     * @api
     * @param float $cost
     */
    public function setCost($cost) {
        $this->cost = (float)$cost;
    }
    
    /**
     * return the product instance identified by sku or throw exception
     */
    public function getProduct() {
        return $this->_product;
    }
    
    /**
     * return the stock item of the product
     */
    public function getStockItem() {
        return $this->_stockItemRepository->get($this->_product->getId());
    }
}