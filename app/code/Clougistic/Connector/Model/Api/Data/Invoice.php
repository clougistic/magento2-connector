<?php namespace Clougistic\Connector\Model\Api\Data;

use Clougistic\Connector\Api\Data\InvoiceInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Convert\Order as OrderConverter;

class Invoice implements InvoiceInterface
{
    private $order_reference;
    private $items = [];
    
    protected $_order;
    protected $_orderFactory;
    protected $_orderConverter;
    
    public function __construct (
        OrderFactory $orderFactory,
        OrderConverter $orderConverter
    ) {
        $this->_orderFactory = $orderFactory->create();
        $this->_orderConverter = $orderConverter->create();
    }
    
    /**
     * @api
     * @return string
     */
    public function getOrderReference() {
        return $this->order_reference;
    }
    
    /**
     * @api
     * @param string $sku
     */
    public function setOrderReference($reference) {
        $this->order_reference = $reference;
        $this->_order = $this->_orderFactory->loadByIncrementId($this->order_reference);
    }

    /**
     * @api
     * @return array
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @api
     * @param array $items
     */
    public function setItems($items) {
        $this->items = $items;
    }
    
    /**
     * get order with order reference
     */
    public function getOrder() {
        return $this->_order;
    }
    
    /**
     * create an invoice from the order
     */
    public function getOrderToInvoice() {
        return $this->_orderConverter->toInvoice($this->_order);
    }
    
    /**
     * create an invoice item from the order item
     *
     * @param $orderItem order item to invoice
     * @param $qty qty to invoice
     */
    public function getOrderItemToInvoiceItem($orderItem, $qty) {
        return $this->_orderConverter->itemToInvoiceItem($orderItem)->setQty($qty);
    }
}