<?php namespace Clougistic\Connector\Model\Api\Data;

use Clougistic\Connector\Api\Data\TestCallbackInterface;
use Clougistic\Connector\Helper\Api as ApiHelper;
use Magento\Framework\Webapi\Exception;
use Clougistic\Connector\Model\Api;

class TestCallback implements TestCallbackInterface
{
    /**
     * @var ApiHelper
     */
    protected $_apiHelper;

    private $host;

    public function __construct(
        ApiHelper $apiHelper
    ) {
        $this->_apiHelper = $apiHelper;
    }

    /**
     * @api
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @api
     * @param string $host
     * @return void
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @api
     * @param \Clougistic\Connector\Api\Data\TestCallbackInterface $data
     * @return \Clougistic\Connector\Model\Api\Response
     */
    public function testCallback($data)
    {
        try {
            if ($data->getHost() === null) {
                throw new Exception(__('Missing Host.'));
            }

            $this->setHost($data->getHost());

            if($this->_apiHelper->getAdminHostname() === $this->getHost()) {
                $this->_apiHelper->setResponse(Api::RESPONSE_STATUS_SUCCESS);
            } else {
                $this->_apiHelper->setResponse(Api::RESPONSE_STATUS_ERROR);
            }
        } catch (Exception $e) {
            $this->_apiHelper->setResponse(Api::RESPONSE_STATUS_ERROR, __($e->getMessage()), $e->getTraceAsString());
        }

        return $this->_apiHelper->getResponse();
    }
}