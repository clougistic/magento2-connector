<?php namespace Clougistic\Connector\Model\Api;

class Response
{
    private $status;
    private $data;
    private $trace;
    
    /**
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param string $status
     * @return null
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getData() {
        return $this->data;
    }

    /**
     * @param string|mixed $data
     * @return null
     */
    public function setData($data) {
        $this->data = $data;
    }
 
    /**
     * @return string
     */
    public function getTrace() {
        return $this->trace;
    }

    /**
     * @param string $trace
     * @return null
     */
    public function setTrace($trace) {
        $this->trace = $trace;
    }
}