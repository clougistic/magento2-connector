<?php namespace Clougistic\Connector\Model\Carrier\Usa\Document;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
class Label
{
    protected $_printNodeConnector;
    protected $_printNodeHelper;
    
    public function __construct(
        \Clougistic\Connector\Model\PrintNode\Connector $printNodeConnector,
        \Clougistic\Connector\Helper\PrintNode\Config $printNodeHelper
    ) {
        $this->_printNodeConnector = $printNodeConnector; 
        $this->_printNodeHelper = $printNodeHelper;
    }
    
	public function sendToPrinter($shipment, $data)
	{
		$printNode = $data->getPrintNode();
        if (!isset($printNode['api_key']) || $data->getCarrierCode() === null) {
			return;
		}

		if (!$this->_printNodeHelper->isEnabled()) {
			return;
		}

        $this->_printNodeConnector->setApiKey($printNode['api_key']);

		$labels = $shipment->getShippingLabel();
		if (strlen($labels) > 0) {
			$printerId = @$printNode['labels'][$data->getCarrierCode()]['*label'];
			if ($printerId) {
				if ($this->_printNodeHelper->canPrintDocument($data->getCarrierCode(), '*label', $shipment->getStore())) {
					$jobTitle = sprintf('Shipment %s %s Label', $shipment->getIncrementId(), $data->getCarrierCode());
					$this->_printNodeConnector->submit(
						base64_encode($labels),
						$printerId, 
						$jobTitle 
					);	
				}
			}
		}
	}
}