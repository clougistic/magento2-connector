<?php namespace Clougistic\Connector\Model\Carrier;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
interface LogicInterface
{
	public function before($shipment, $data);
	public function afterShipment($shipment, $data);
	
	public function getOrderOptionData($order);
	public function getDocuments();
}