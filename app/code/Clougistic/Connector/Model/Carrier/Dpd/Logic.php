<?php namespace Clougistic\Connector\Model\Carrier\Dpd;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
use Clougistic\Connector\Model\Carrier\Dpd\Document\Label;
use Magento\Framework\Webapi\Exception;
 
class Logic implements \Clougistic\Connector\Model\Carrier\LogicInterface
{
    protected $_label;
    
    public function __construct(
        Label $label
    ) {
        $this->_label = $label;
    }
    
    public function getOrderOptionData($order)
    {
		return null;
    }
	
	public function getDocuments()
	{
		return [
			['value' => 'dpdclassic_*label', 'label' => 'DPD Classic Label']
		];
	}
	
	public function before($shipment, $data)
	{
		if (!is_numeric($data->getParcelCount())) {
			throw new Exception(__(sprintf('Invalid value for Parcel Count [%s]', $data->getParcelCount())));
		}
		
		# register dpd data which normally would come from the shipment creation page.
		# ie. parcel_count
		
		$shipment->setDpdPackageCount($data->getParcelCount());
	}
	
	public function afterShipment($shipment, $data) 
	{
		# print dpd label
		$this->_label->sendToPrinter($shipment, $data);
	}
}
