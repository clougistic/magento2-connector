<?php namespace Clougistic\Connector\Model\Carrier\Dpd\Document;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
class Label
{    
    protected $_printNodeConnector;
    protected $_printNodeHelper;
    protected $_messageManager;
    protected $_dpdGrid;
    
    public function __construct(
        \Clougistic\Connector\Model\PrintNode\Connector $printNodeConnector,
        \Clougistic\Connector\Helper\PrintNode\Config $printNodeHelper,
        # \DPD\Shipping\Model\Adminhtml\Dpdgrid $dpdGrid,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_printNodeConnector = $printNodeConnector; 
        $this->_printNodeHelper = $printNodeHelper;
        $this->_dpdGrid = $objectManager->get('DPD\Shipping\Model\Adminhtml\Dpdgrid'); //$dpdGrid;
        $this->_messageManager = $messageManager;
    }
	public function sendToPrinter($shipment, $data)
	{
		$printNode = $data->getPrintNode();
        if (!isset($printNode['api_key']) || $data->getCarrierCode() === null) {
			return;
		}

		if (!$this->_printNodeHelper->isEnabled()) {
			return;
		}

        $this->_printNodeConnector->setApiKey($printNode['api_key']);
		
		$result = $this->_dpdGrid->generateLabelAndCompleteOrder($shipment->getOrder(), $shipment);
		if ($result) {
			if (strlen($shipment->getDpdShippingLabel()) > 0) {
				$printerId = @$printNode['labels'][$data->getCarrierCode()]['*label'];
				if ($printerId) {
					if ($this->_printNodeHelper->canPrintDocument($data->getCarrierCode(), '*label', $shipment->getStore())) {
						$jobTitle = sprintf('Shipment %s %s Label', $shipment->getIncrementId(), $data->getCarrierCode());
						$this->_printNodeConnector->submit(
							base64_encode($shipment->getDpdShippingLabel()),
							$printerId, 
							$jobTitle 
						);	
					}
				}
			}
		}
		else {
			foreach ($this->_messageManager->getMessages()->getItems() as $message) {
				$output .= $message->getText() . "<br>\r\n";
			}
			throw new Exception($output);
		}
	}
}