<?php namespace Clougistic\Connector\Model\Carrier;

class Selector
{
    const CONFIG_PATH_MODULES = 'clougistic_connector/general/modules';
    
    protected $_moduleManager = null;
    protected $_scopeConfig = null;
    
    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_scopeConfig = $scopeConfig;
    }
    
    public function getAllDocuments()
	{
		$modules = $this->_scopeConfig->getValue(self::CONFIG_PATH_MODULES, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$documents = $this->getStandardDocuments();
       
		foreach ($modules as $moduleKey => $module) {
			if ($this->_moduleManager->isEnabled($moduleKey)) {
				$logicModel = new $module['model'];
				$documents = array_merge($documents, $logicModel->getDocuments());
			}
		}
		
		return $documents;
	}
    
    public function getStandardDocuments()
	{
		return [
            ['value' => 'mage_*invoice', 	 'label' => 'Magento Invoice'],
			['value' => 'mage_*packingslip', 'label' => 'Magento Packingslip']
		];
	}
}