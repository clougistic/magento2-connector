<?php namespace Clougistic\Connector\Model\Carrier\Standard;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
class Logic implements \Clougistic\Connector\Model\Carrier\LogicInterface
{
    public function getOrderOptionData($order)
    {
		return null;
    }

	public function getDocuments()
	{
		return [
			['value' => 'mage_*invoice',     'label' => 'Magento Invoice'],
			['value' => 'mage_*packingslip', 'label' => 'Magento Packingslip']
		];
	}
	
	public function before($shipment, $data) {}
	
	public function afterShipment($shipment, $data) {}
}
