<?php namespace Clougistic\Connector\Model\Carrier\Standard\Document;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
class PackingSlip
{
    protected $_printNodeConnector;
    protected $_printNodeHelper;
    protected $_orderPdfShipment;
    
    public function __construct(
        \Clougistic\Connector\Model\PrintNode\Connector $printNodeConnector,
        \Clougistic\Connector\Helper\PrintNode\Config $printNodeHelper,
        \Magento\Sales\Model\Order\Pdf\Shipment $orderPdfShipment
    ) {
        $this->_printNodeConnector = $printNodeConnector; 
        $this->_printNodeHelper = $printNodeHelper;
        $this->_orderPdfShipment = $orderPdfShipment;
    }
    
	public function sendToPrinter($shipment, $data)
	{
    	$printNode = $data->getPrintNode();
        if (!isset($printNode['api_key']) || $data->getCarrierCode() === null) {
			return;
		}

		if (!$this->_printNodeHelper->isEnabled()) {
			return;
		}
		
		if (!$this->_printNodeHelper->canPrintDocument('mage', '*packingslip', $shipment->getStore())) {
			return;
		}

        $this->_printNodeConnector->setApiKey($printNode['api_key']);

		$pdf = $this->_orderPdfShipment->getPdf([$shipment]);
		if ($pdf) {
			$printerId = @$printNode['labels']['mage']['*packingslip'];
			if ($printerId) {
				$jobTitle = sprintf('Shipment %s Standard PackingSlip', $shipment->getIncrementId());
				$this->_printNodeConnector->submit(
					base64_encode($pdf->render()),
					$printerId, 
					$jobTitle
				);	
			}
		}
	}
}