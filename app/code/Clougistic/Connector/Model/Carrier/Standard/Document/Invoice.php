<?php namespace Clougistic\Connector\Model\Carrier\Standard\Document;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
class Invoice
{
    protected $_printNodeConnector;
    protected $_printNodeHelper;
    protected $_orderPdfInvoice;
    
    public function __construct(
        \Clougistic\Connector\Model\PrintNode\Connector $printNodeConnector,
        \Clougistic\Connector\Helper\PrintNode\Config $printNodeHelper,
        \Magento\Sales\Model\Order\Pdf\Invoice $orderPdfInvoice
    ) {
        $this->_printNodeConnector = $printNodeConnector; 
        $this->_printNodeHelper = $printNodeHelper;
        $this->_orderPdfInvoice = $orderPdfInvoice;
    }
    
	public function sendToPrinter($invoice, $data)
	{
    	$printNode = $data->getPrintNode();
        if (!isset($printNode['api_key']) || $data->getCarrierCode() === null) {
			return;
		}

		if (!$this->_printNodeHelper->isEnabled()) {
			return;
		}
			
		if (!$this->_printNodeHelper->canPrintDocument('mage', '*invoice', $invoice->getStore())) {
			return;
		}

        $this->_printNodeConnector->setApiKey($printNode['api_key']);
		
		$pdf = $this->_orderPdfInvoice->getPdf([$invoice]);
		if ($pdf) {
			$printerId = @$printNode['labels']['mage']['*invoice'];
			if ($printerId) {
				$jobTitle = sprintf('Invoice %s Standard', $invoice->getIncrementId());
				$this->_printNodeConnector->submit(
					base64_encode($pdf->render()),
					$printerId, 
					$jobTitle
				);	
			}
		}
	}
}