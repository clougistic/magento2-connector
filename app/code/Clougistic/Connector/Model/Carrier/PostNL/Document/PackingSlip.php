<?php namespace Clougistic\Connector\Model\Carrier\PostNL\Document;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
class PackingSlip #extends \TIG\PostNL\Model\Core\PackingSlip
{
    protected $_printNodeConnector;
    protected $_printNodeHelper;
    protected $_postNLHelper;
    protected $_postNLShipmentService;
    
    public function __construct(
        \Clougistic\Connector\Model\PrintNode\Connector $printNodeConnector,
        \Clougistic\Connector\Helper\PrintNode\Config $printNodeHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager
        #\TIG\PostNL\Helper\Carrier $postNLHelper,
        #\TIG\PostNL\Model\Core\Service\Shipment $postNLShipmentService
    ) {
        $this->_printNodeConnector = $printNodeConnector; 
        $this->_printNodeHelper = $printNodeHelper;
        $this->_postNLHelper = $objectManager->get('TIG\PostNL\Helper\Data');
        #$this->_postNLShipmentService = $postNLShipmentService;
    }
    
	public function sendToPrinter($shipment, $data)
	{
        $printNode = $data->getPrintNode();
        if (!isset($printNode['api_key']) || $data->getCarrierCode() === null) {
			return;
		}

		if (!$this->_printNodeHelper->isEnabled()) {
			return;
		}
		
		if (!$this->_postNLHhelper->isPostNLOrder($shipment->getOrder())) {
			return;
		}
        		
		if (!$this->_printNodeHelper->canPrintDocument($options->carrier_code, '*packingslip', $shipment->getStore())) {
			return;
		}
		
        $this->_printNodeConnector->setApiKey($printNode['api_key']);
        
        # use postnl api?
		#$postnlShipment = $this->_postNLShipmentService->loadShipment($shipment->getId(), true);
		$pdf = parent::_getPackingSlipPdf($postnlShipment);
        
		if ($pdf) {
			$printerId = @$printNode['labels'][$data->getCarrierCode()]['*packingslip'];
			if ($printerId) {
				$jobTitle = sprintf('Shipment %s PostNL PackingSlip', $shipment->getIncrementId());
				$this->_printNodeConnector->submit(
					base64_encode($pdf->render()),
					$printerId, 
					$jobTitle
				);	
			}
		}
	}
}