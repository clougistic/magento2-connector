<?php namespace Clougistic\Connector\Model\Carrier\PostNL\Document;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */
class Label
{
    protected $_printNodeConnector;
    protected $_printNodeHelper;
    protected $_postNLHelper;
    protected $_postNLShipmentService;
    
    public function __construct(
        \Clougistic\Connector\Model\PrintNode\Connector $printNodeConnector,
        \Clougistic\Connector\Helper\PrintNode\Config $printNodeHelper,
        \TIG\PostNL\Helper\Carrier $postNLHelper,
        \TIG\PostNL\Model\Core\Service\Shipment $postNLShipmentService
    ) {
        $this->_printNodeConnector = $printNodeConnector; 
        $this->_printNodeHelper = $printNodeHelper;
        $this->_postNLHelper = $postNLHelper;
        $this->_postNLShipmentService = $postNLShipmentService;
    }
    
	public function sendToPrinter($shipment, $data)
	{
		$printNode = $data->getPrintNode();
        if (!isset($printNode['api_key']) || $data->getCarrierCode() === null) {
			return;
		}

		if (!$this->_printNodeHelper->isEnabled()) {
			return;
		}
		
		if (!$this->_postNLHhelper->isPostnlShippingMethod($shipment->getOrder()->getShippingMethod())) {
			return;
		}

		$includeReturnLabels = $this->_postNLHelper->canPrintReturnLabelsWithShippingLabels($shipment->getStoreId());
		$labels = $this->_postNLShipmentService->getLabels($shipment, true, $includeReturnLabels);

        $this->_printNodeConnector->setApiKey($printNode['api_key']);
        
		$labelCounter = 1;
		foreach ($labels as $label) {		
			$printerId = @$printNode['labels'][$data->getCarrierCode()][$label->getLabelType()];
			if ($printerId) {
				if ($this->_printNodeHelper->canPrintDocument($data->getCarrierCode(), $label->getLabelType(), $shipment->getStore())) {
					$jobTitle = sprintf('Shipment %s %s %s (%s)', $shipment->getIncrementId(), $data->getCarrierCode(), $label->getLabelType(), $labelCounter++);
					$this->_printNodeConnector->submit(
						$label->getLabel(),  # labels are base64 encoded already by PostNL
						$printerId, 
						$jobTitle 
					);	
				}
			}
		}
	}
}