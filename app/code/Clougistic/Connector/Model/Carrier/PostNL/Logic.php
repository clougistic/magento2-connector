<?php namespace Clougistic\Connector\Model\Carrier\PostNL;
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 */

use Magento\Framework\Webapi\Exception;
 
class Logic implements \Clougistic\Connector\Model\Carrier\LogicInterface
{
    const ORDER_FIELD_TYPE = 'type';
    const ORDER_FIELD_PRODUCT_CODE = 'product_code';
    const ORDER_FIELD_DELIVERY_DATE = 'delivery_date';
    const ORDER_FIELD_CONFIRM_DATE = 'confirm_date';
    const ORDER_FIELD_IS_PAKJEGEMAK = 'is_pakje_gemak';
    const ORDER_FIELD_IS_PAKKETAUTOMAAT = 'is_pakketautomaat';

    protected $_label;
    protected $_packingSlip;
    protected $_postNLOrderRepository;
    protected $_registry;
    
    public function __construct(
        \Clougistic\Connector\Model\Carrier\PostNL\Document\Label $label,
        \Clougistic\Connector\Model\Carrier\PostNL\Document\PackingSlip $packingSlip,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Registry $registry
    ) {
        $this->_label = $label;
        $this->_packingSlip = $packingSlip;
        $this->_postNLOrderRepository = $objectManager->get('TIG\PostNL\Api\OrderRepositoryInterface');
        $this->_registry = $registry;
    }
    
    public function getOrderOptionData($order)
    {
		#$orderPostNL = Mage::getModel('postnl_core/order')->load($order->getEntityId(), 'order_id');
		#$orderPostNL = $this->_orderPostNL->load($order->getEntityId(), 'order_id');
		$orderPostNL = $this->_postNLOrderRepository->getById($order->getEntityId());
		
		if ($orderPostNL->getEntityId()) { 
			return [
                self::ORDER_FIELD_TYPE => $orderPostNL->getType(),
				self::ORDER_FIELD_PRODUCT_CODE => $orderPostNL->getProductCode(),
				self::ORDER_FIELD_DELIVERY_DATE => $orderPostNL->getDeliveryDate(),
				self::ORDER_FIELD_CONFIRM_DATE => $orderPostNL->getConfirmDate(),
				self::ORDER_FIELD_IS_PAKJEGEMAK => $orderPostNL->getIsPakjeGemak(),
				self::ORDER_FIELD_IS_PAKKETAUTOMAAT => $orderPostNL->getIsPakketautomaat()
			];
		}
		
		return null;
    }

	public function getDocuments()
	{
		return [
			['value' => 'postnl_*packingslip',  'label' => __('PostNL Packingslip')],
			['value' => 'postnl_label', 		'label' => __('PostNL Label')],
			['value' => 'postnl_label-combi', 	'label' => __('PostNL Label-combi')],
			['value' => 'postnl_codcard', 		'label' => __('PostNL COD Card')],
			['value' => 'postnl_return_label',  'label' => __('PostNL Return Label')]
		];
	}
	
	public function before($shipment, $data)
	{
		if (!is_numeric($data->getParcelCount())) {
			throw new Exception(__(sprintf('Invalid value for Parcel Count [%s]', $data->getParcelCount())));
		}
		
		# register postnl data which normally would come from the shipment creation page.
		# ie. parcel_count, product_option and extra_cover_amount
	
		$carrierOptions = $data->getCarrierOptions();
        $this->_registry->register('postnl_product_option', $carrierOptions['product_option']);
		
		unset($carrierOptions['product_option']);
		
		$this->_registry->register('postnl_additional_options', $carrierOptions);
	}
	
	public function afterShipment($shipment, $data) 
	{
		# print postnl labels and packingslip
		$this->_label->sendToPrinter($shipment, $data);
		$this->_packingSlip->sendToPrinter($shipment, $data);
	}
}
