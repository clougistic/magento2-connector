<?php namespace Clougistic\Connector\Block\Adminhtml\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;
 
class Version extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_helper = null;
    
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Clougistic\Connector\Helper\Data $helper,
        array $data = []
    ) {
        $this->_helper = $helper;
        
        parent::__construct($context, $data);
    }
        
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_helper->getExtensionVersion();
    }
}