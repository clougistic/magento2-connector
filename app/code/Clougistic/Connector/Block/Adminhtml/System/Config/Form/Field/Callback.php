<?php namespace Clougistic\Connector\Block\Adminhtml\System\Config\Form\Field;

use Clougistic\Connector\Model\Api;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;

class Callback extends Field
{
    protected function _getElementHtml(AbstractElement $element)
    {
        $this->setElement($element);

        return
            <<<CBUTTON
    <button id="clougistic_wms_connector_test_callback" title="Test callback" type="button" style=""><span><span><span>Test Clougistic Callback</span></span></span></button>
    <div id="clougistic_wms_connector_test_callback_popup_modal"></div>
    <script type="text/javascript">
    require(['jquery','Magento_Ui/js/modal/modal'],    
        function($,modal) {
        
            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                title: $.mage.__('Test callback'),
                modalClass: 'see-detail-modal',
                buttons: [{
                    text: $.mage.__('Close'),
                    class: 'product-popup-hide',
                    click: function () {
                        this.closeModal();
                    }
                }]
            };
            
            $('body').delegate('#clougistic_wms_connector_test_callback', 'click', function (e) {         
                var testPopupModalSelector = '#clougistic_wms_connector_test_callback_popup_modal';
                modal(options, $(testPopupModalSelector));                
                $.ajax({
                       type: "GET",
                       url:  "{$this->getUrl('clougistic_connector/clougistic/testCallback')}",                   
                       showLoader: true,
                       success: function(data)
                       {
                           $(testPopupModalSelector).html(data);
                           $(testPopupModalSelector).modal('openModal');                       
                       },
                       error: function (message) {
                           $(testPopupModalSelector).html(message);
                           $(testPopupModalSelector).modal('openModal');                       
                       },
                });            
            });
        }
    );
    </script>

CBUTTON;
    }

    /**
     * Render button
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        /* Remove scope label */
        $element->unsLabel()->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }
}