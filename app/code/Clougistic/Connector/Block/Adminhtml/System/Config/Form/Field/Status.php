<?php namespace Clougistic\Connector\Block\Adminhtml\System\Config\Form\Field;

use Clougistic\Connector\Model\Api;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;

class Status extends Field
{
    protected $_api;
    
    public function __construct(
        Context $context,
        Api $api,
        array $data = []
    ) {
        $this->_api = $api;
        
        parent::__construct($context, $data);
    }
        
    protected function _getElementHtml(AbstractElement $element)
    {
        $systemInfo = null;
        $greetings = $this->_api->greetings($systemInfo);
        if ($greetings) {

            $systemInfoText = '';
            foreach ($systemInfo as $key => $value) {
                $systemInfoText .= sprintf("%s: %s\n", $key, $value);
            }

            return '<label style="padding: 2px; background-color: black; color: #00ff00; display: block">Connected</label>'
                . sprintf('<textarea style="font-family: consolas; height: 150px; height:overflow-x: hidden; overflow-y: scroll"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" readonly="readonly">%s</textarea>', $systemInfoText);
        }
        else {
            return '<label style="padding: 2px; background-color: black; color: #ff0000; display: block">Not connected</label>';
        }
    }
}