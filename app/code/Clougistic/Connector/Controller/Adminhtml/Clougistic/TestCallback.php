<?php

namespace Clougistic\Connector\Controller\Adminhtml\Clougistic;

use Clougistic\Connector\Model\Api;

/**
 * Class TestCallback
 * @package Clougistic\Connector\Controller\Adminhtml\Clougistic
 */
class TestCallback extends \Clougistic\Connector\Controller\Adminhtml\Clougistic
{
    /**
     * @var Api
     */
    protected $_api;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param Api $api
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Api $api
    ) {
        parent::__construct($context);
        $this->_api = $api;
    }

    /**
     * Test callback from Clougistic
     * This is used to check if the export api in Clougistic has been setup correctly
     * to enable bidirectional requests
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        echo $this->_api->testCallback();
    }
}
